module.exports = () => {
  return async () => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "https://www.teamrockstars.nl/sites/default/files/db.json";

    try {
      let response = await fetch(proxyurl + url);
      let data = await response.json();

      this.setState({
        songs: [...this.state.songs, ...data.songs],
        artists: [...this.state.artists, ...data.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };
};
