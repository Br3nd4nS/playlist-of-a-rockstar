import React, { Component } from "react";
import PageTitle from "../components/Global/PageTitle/PageTitle";
import Button from "../components/Button/Button";

import Form from "../components/Form/Form";

class PlaylistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playlists: []
    };

    this.formProps = {
      formPurpose: "make-playlist",
      formPlaceholder: "Naam van nieuwe playlist"
    };

    this.buttonProps = {
      text: "Add new playlist",
      type: "button",
      iconClassName: "fas fa-forward",
      className: "btn btn--yellow",
      dataToggle: "modal",
      dataTarget: "#exampleModal"
    };
  }

  addNewPlaylist = e => {};

  render() {
    const title = "Playlists";

    return (
      <div className="container">
        <PageTitle title={title} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form formProps={this.formProps} />

            <Button props={this.buttonProps} />
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
