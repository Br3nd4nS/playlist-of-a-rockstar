import React from "react";
import { BrowserRouter as Link, Route, NavLink } from "react-router-dom";

import ArtistsView from "./Artists";
import PlaylistsView from "./Playlists";
import SongsView from "./Songs";

import routes from "../routing/routes";

const HomeView = ({ artists, songs, playlists }) => {
  const pageBlocks = routes.map(({ path, view }, key) => {
    return path !== "/" ? (
      <div className="col-4" key={key}>
        <div className="pageBlock">
          <NavLink
            to={path}
            activeClassName="isActive txt--color-white"
            className="pageBlock__link"
          >
            <div className="pageBlock">
              <i className="fas fa-music" />

              {view}
            </div>
          </NavLink>
        </div>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
