import React from "react";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";

import ArtistsView from "./Artists";
import PlaylistsView from "./Playlists";
import SongsView from "./Songs";

import routes from "../routing/routes";

const HomeView = () => {
  const pageBlocks = routes.map(({ path, view }, key) => {
    return path !== "/" ? (
      <div className="col-4" key={key}>
        <div className="pageBlock">
          <Link
            to={path}
            activeClassName="isActive txt--color-white"
            className="pageBlock__link"
          >
            <i className="fas fa-music" />

            {view}
          </Link>

          <Switch>
            <Route exact path="/" render={() => <HomeView />} />
            <Route
              path="/artists"
              render={() => <ArtistsView fetchData={this.fetchData} />}
            />
            <Route
              path="/songs"
              render={() => <SongsView fetchData={this.fetchData} />}
            />
            <Route path="/playlists" render={() => <PlaylistsView />} />
          </Switch>
        </div>
      </div>
    ) : null;
  });

  return (
    <Router>
      <div className="container">
        <div className="row">{pageBlocks}</div>
      </div>
    </Router>
  );
};

export default HomeView;
