import React from "react";
import { BrowserRouter as NavLink } from "react-router-dom";

import routes from "../routing/routes";

const HomeView = () => {
  const pageBlocks = routes.map(({ path, view, icon }, key) => {
    return path !== "/home" ? (
      <div className="col-4" key={key} match>
        <NavLink to={path} className="pageblock-wrapper">
          <div className="pageblock flex flex--center-v flex--center-h">
            {/* <h3>{view}</h3> */}

            <i className={icon} />
          </div>
        </NavLink>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
