import React, { Component } from "react";
import SearchForm from "../components/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";
import _ from "lodash";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "",
      songs: [],
      artists: [],
      filteredArtists: []
    };
  }

  debounceInput() {
    _.debounce(() => {
      this.searchDebounced = this.state.searchTerm;
    }, 250);
  }

  handleChange = e => {
    let inputValue = e.target.value;
    this.setState({ searchTerm: inputValue });
  };

  search = (stringToCheck, searchTerm) => {
    return stringToCheck.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
  };

  filteredArtists = e => {
    const artistsArray = this.state.artists;
    const test = artistsArray.filter(artist => {
      const artistName = artist.name;
      const term = this.state.searchTerm.split("");

      return term.every(term => this.search(artistName, term));
    });

    this.setState({
      filteredArtists: [...this.state.artists, ...test]
    });
    console.log(test);
  };

  fetchSongs = async url => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);
      let data = await response.json();

      this.setState({
        songs: [...this.state.songs, ...data.songs],
        artists: [...this.state.artists, ...data.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  componentDidUpdate() {}

  render() {
    let { searchTerm, artists, filteredArtists } = this.state;

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Artists</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <SearchForm
              searchTerm={searchTerm}
              handleChange={this.handleChange}
              filteredArtists={this.filteredArtists}
            />

            <SearchResults
              artists={artists}
              filteredArtists={filteredArtists}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
