import React from "react";

import routes from "../routing/routes";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

const HomeView = () => {
  const pageBlocks = routes.map(({ url, view }, key) => {
    return url !== "/" ? (
      <div className="col-4" key={key}>
        <div className="pageBlock">
          <NavLink
            to={url}
            activeClassName="isActive txt--color-white"
            className="pageBlock__link"
          >
            {view}
          </NavLink>
        </div>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
