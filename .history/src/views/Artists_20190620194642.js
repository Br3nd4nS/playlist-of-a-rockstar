import React from "react";

const ArtistsView = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-6 offset-sm-3">
          <form action="">
            <div className="form-group">
              <label htmlFor="searchArtist">Email address</label>
              <input
                type="text"
                className="form-control"
                id="searchArtist"
                aria-describedby="searchArtist"
                placeholder="Zoek je favoriete artiest"
              />
            </div>
          </form>
        </div>
      </div>
      <h1>Artists</h1>
    </div>
  );
};

export default ArtistsView;
