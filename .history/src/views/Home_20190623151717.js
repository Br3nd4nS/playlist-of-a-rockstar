import React from "react";
import { BrowserRouter as Link, Route, NavLink } from "react-router-dom";

import ArtistsView from "./Artists";
import PlaylistsView from "./Playlists";
import SongsView from "./Songs";

import routes from "../routing/routes";

const HomeView = ({ artists, songs, playlists }) => {
  const pageBlocks = routes.map(({ path, icon }, key) => {
    return path !== "/" ? (
      <div className="col-4" key={key}>
        <NavLink to={path} className="pageblock-wrapper">
          <div className="pageblock flex flex--center-v flex--center-h">
            <h3>{view}</h3>

            <i className={icon} />
          </div>
        </NavLink>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
