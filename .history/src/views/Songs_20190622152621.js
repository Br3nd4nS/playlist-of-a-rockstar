import React, { Component } from "react";

import Form from "../components/shared/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";
import PageTitle from "../components/shared/PageTitle/PageTitle";

class Songs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: "",
      filteredSongs: []
    };

    this.formProps = {
      formPurpose: "search-song",
      formPlaceholder: "Zoek je favoriete nummer"
    };

    this.pageTitle = "Songs";
  }

  // Async call to fetch the data for displaying artists
  /* fetchSongs = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      // Put the fetched data into the artists state array, using spread. This it is a non-destructive approach of assigning data into state.
      this.setState({
        songs: [...this.state.songs, ...data]
      });
    } catch (error) {
      console.log(error);
    }
  }; */

  handleChange = e => {
    let searchValue = e.target.value;

    let currentSongs = [];
    let updatedSongs = [];

    if (searchValue !== "") {
      currentSongs = this.props.songs;

      updatedSongs = currentSongs.filter(song => {
        const songName = song.name.toLowerCase();

        // change search term to lowercase
        const inputValue = searchValue.toLowerCase();
        // check to see if the current list item includes the search term
        // If it does, it will be added to newList. Using lowercase eliminates
        // issues with capitalization in search terms and search content
        return songName.includes(inputValue);
      });
    } else {
      updatedSongs = this.props.songs;
    }

    this.setState({
      inputValue: searchValue,
      filteredSongs: updatedSongs
    });
  };

  componentDidMount() {
    this.fetchSongs("http://localhost:3000/songs");
  }

  render() {
    let { inputValue, songs, filteredSongs } = this.state;

    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form
              formProps={this.formProps}
              inputValue={inputValue}
              handleChange={this.handleChange}
              filteredArtists={this.filteredSongs}
            />

            <SearchResults artists={songs} filteredArtists={filteredSongs} />
          </div>
        </div>
      </div>
    );
  }
}

export default Songs;
