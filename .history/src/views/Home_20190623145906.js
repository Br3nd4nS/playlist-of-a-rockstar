import React from "react";
import { BrowserRouter as Link, Route, NavLink } from "react-router-dom";

import ArtistsView from "./Artists";
import PlaylistsView from "./Playlists";
import SongsView from "./Songs";

import routes from "../routing/routes";

const HomeView = ({ artists, songs, playlists }) => {
  const pageBlocks = routes.map(({ path, view }, key) => {
    return path !== "/" ? (
      <div className="col-4" key={key}>
        <NavLink to={path} className="pageBlock__link">
          <div className="pageBlock flex flex--center-v flex--center-h">
            {/* {view} */}

            <i className="fas fa-music" />
          </div>
        </NavLink>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
