import React, { Component } from "react";
import Form from "../components/shared/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";
import PageTitle from "../components/shared/PageTitle/PageTitle";
import _ from "lodash";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      filteredArtists: []
    };

    this.formProps = {
      formPurpose: "search-artist",
      formPlaceholder: "Zoek je favoriete artiest"
    };

    this.pageTitle = "Artists";
  }

  // Use debounce tot preserve performance.
  debounceInput() {
    _.debounce(() => {
      this.searchDebounced = this.state.searchTerm;
    }, 250);
  }

  handleChange = e => {
    let searchValue = e.target.value;

    let currentArtists = [];
    let updatedArtists = [];

    if (searchValue !== "") {
      currentArtists = this.props.artists;

      updatedArtists = currentArtists.filter(artist => {
        const artistName = artist.name.toLowerCase();

        // change search term to lowercase
        const inputValue = searchValue.toLowerCase();
        // check to see if the current list item includes the search term
        // If it does, it will be added to newList. Using lowercase eliminates
        // issues with capitalization in search terms and search content
        return artistName.includes(inputValue);
      });
    } else {
      updatedArtists = this.props.artists;
    }

    this.setState({
      inputValue: searchValue,
      filteredArtists: updatedArtists
    });
  };

  componentDidMount() {
    this.props.fetchData("http://localhost:3000/artists");
  }

  /* componentWillReceiveProps(nextProps) {
    this.setState({
      filteredArtists: nextProps.artists
    });
  } */

  componentDidUpdate() {
    console.log(this.props.match);
  }

  render() {
    let { inputValue, artists, filteredArtists } = this.state;

    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form
              formProps={this.formProps}
              inputValue={inputValue}
              handleChange={this.handleChange}
              filteredArtists={this.filteredArtists}
            />

            <SearchResults
              path={this.props.match}
              artists={artists}
              filteredArtists={filteredArtists}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
