import React, { Component } from "react";
import Form from "../components/shared/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";
import PageTitle from "../components/shared/PageTitle/PageTitle";
import _ from "lodash";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      artists: this.props.artists,
      filteredArtists: this.props.artists
    };

    this.formProps = {
      formPurpose: "search-artist",
      formPlaceholder: "Zoek je favoriete artiest"
    };

    this.pageTitle = "Artists";
  }

  // Use debounce tot preserve performance.
  debounceInput() {
    _.debounce(() => {
      this.searchDebounced = this.state.searchTerm;
    }, 250);
  }

  handleChange = e => {
    let searchValue = e.target.value;

    let currentArtists = [];
    let updatedArtists = [];

    /* if (searchValue !== "") { */
    currentArtists = this.props.artists;

    updatedArtists = currentArtists.filter(artist => {
      // put all letters in artist array to lowercase letters
      const artistName = artist.name.toLowerCase();

      // put all letters in searchterm to lowercase letters
      const inputValue = searchValue.toLowerCase();

      // check if the artist array has something which is identical to the inputValue and put this into updatedArtists
      return artistName.includes(inputValue);
    });
    /* } else {
      updatedArtists = this.props.artists;
    } */

    this.setState({
      inputValue: searchValue,
      filteredArtists: updatedArtists
    });
  };

  static getDerivedStateFromProps(props, state) {
    // Any time the current user changes,
    // Reset any parts of state that are tied to that user.
    // In this simple example, that's just the email.
    if (props.artists !== state.artists) {
      return {
        artists: props.artists,
        filteredArtists: props.filteredArtists
      };
    }
    return null;
  }

  /* static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.someValue !== prevState.someValue) {
      return { someState: nextProps.someValue };
    } else return null;
  } */

  componentDidMount() {
    this.props.fetchData("http://localhost:3000/artists");
  }

  componentDidUpdate() {}

  render() {
    let { inputValue, artists, filteredArtists } = this.state;

    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form
              formProps={this.formProps}
              inputValue={inputValue}
              handleChange={this.handleChange}
              filteredArtists={this.filteredArtists}
            />

            <SearchResults
              path={this.props.match}
              artists={artists}
              filteredResults={filteredArtists}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
