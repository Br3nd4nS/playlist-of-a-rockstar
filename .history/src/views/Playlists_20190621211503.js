import React, { Component } from "react";
import PageTitle from "../components/shared/PageTitle/PageTitle";
import Button from "../components/shared/Button/Button";
import Form from "../components/shared/Form/Form";
import SavedPlaylists from "../components/SavedPlaylists/SavedPlaylists";

class PlaylistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      playlists: []
    };

    this.pageTitle = "Playlists";

    this.formProps = {
      formPurpose: "create-playlist",
      formPlaceholder: "Naam van nieuwe playlist"
    };

    this.buttonProps = {
      text: "Add new playlist",
      type: "button",
      iconClassName: "fas fa-forward",
      className: "btn btn--yellow"
    };
  }

  handleChange = e => {
    let inputValue = e.target.value;

    this.setState({
      inputValue
    });
  };

  addNewPlaylist = e => {
    const playlistName = this.name.inputValue;
    const newPlaylist = {
      name: playlistName,
      songs: []
    };

    this.setState({
      playlists: [...this.state.playlists, newPlaylist]
    });
  };

  render() {
    const { playlists } = this.state;
    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form handleChange={this.handleChange} formProps={this.formProps} />

            <Button
              props={this.buttonProps}
              addNewPlaylist={this.addNewPlaylist()}
            />

            <SavedPlaylists playlists={playlists} />
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
