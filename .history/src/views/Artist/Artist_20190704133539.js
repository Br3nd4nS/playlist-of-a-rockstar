import React, { Fragment, Component } from "react";
import PageTitle from "../../globalComponents/__elements/PageTitle/PageTitle";


componentDidMount() {
  this.props.fetchData("http://localhost:3000/artists");
  console.log(this.props);
}

class ArtistView extends Component {
  render() {
    const pageTitle = "Test";

    return (
      <Fragment>
        <PageTitle pageTitle={pageTitle} />
  
        <div className="row">
          <div className="col-6" />
        </div>
      </Fragment>
    );
  }
};

export default ArtistView;
