import React, { Fragment, Component } from "react";
import PageTitle from "../../globalComponents/__elements/PageTitle/PageTitle";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

class ArtistView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props);
    return (
      <Fragment>
        <div className="container">
          <PageTitle title={this.props.parsedQueryPath.name} />
          <div className="row">
            <div className="col-6">
              <div className="artist" />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ArtistView;
