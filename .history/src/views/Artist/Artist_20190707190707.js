import React, { Fragment } from "react";
import PageTitle from "../../globalComponents/__elements/PageTitle/PageTitle";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import queryString from "query-string";

const ArtistView = ({ name, props }) => {
  const pageTitle = "Test";
  console.log(props);
  const parsedQueryPath = queryString.parse(window.location.search);

  return (
    <Fragment>
      <PageTitle pageTitle={pageTitle} />

      {parsedQueryPath}

      <div className="container">
        <div className="row">
          <div className="col-6 artist" />
        </div>
      </div>
    </Fragment>
  );
};

export default ArtistView;
