import React, { Fragment } from "react";
import PageTitle from "../../globalComponents/shared/PageTitle/PageTitle";

const ArtistView = () => {
  const pageTitle = "Test";
  return (
    <Fragment>
      <PageTitle pageTitle={pageTitle} />

      <div className="row">
        <div className="col-6" />
      </div>
    </Fragment>
  );
};

export default ArtistView;
