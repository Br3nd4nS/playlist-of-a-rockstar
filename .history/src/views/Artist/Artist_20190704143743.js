import React, { Fragment, Component } from "react";
import PageTitle from "../../globalComponents/__elements/PageTitle/PageTitle";

const ArtistView = () => {
  const pageTitle = "Test";

  return (
    <Fragment>
      <PageTitle pageTitle={pageTitle} />

      <div className="row">
        <div className="col-6" />
      </div>
    </Fragment>
  );
};

export default ArtistView;
