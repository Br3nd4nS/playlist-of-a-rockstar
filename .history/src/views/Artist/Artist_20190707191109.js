import React, { Fragment } from "react";
import PageTitle from "../../globalComponents/__elements/PageTitle/PageTitle";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

const ArtistView = ({ name, props, parsedQueryPath }) => {
  const pageTitle = "Test";
  console.log("test " + parsedQueryPath);

  return (
    <Fragment>
      <PageTitle pageTitle={pageTitle} />

      <div className="container">
        <div className="row">
          <div className="col-6 artist" />
        </div>
      </div>
    </Fragment>
  );
};

export default ArtistView;
