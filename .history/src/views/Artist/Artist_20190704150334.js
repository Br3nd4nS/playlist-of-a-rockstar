import React, { Fragment } from "react";
import PageTitle from "../../globalComponents/__elements/PageTitle/PageTitle";

const ArtistView = () => {
  console.log("hallo");
  const pageTitle = "Test";

  return (
    <Fragment>
      <PageTitle pageTitle={pageTitle} />

      <div className="row">
        <div className="col-6 artist" />
      </div>
    </Fragment>
  );
};

export default ArtistView;
