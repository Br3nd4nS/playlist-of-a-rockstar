import React, { Fragment, Component } from "react";
import PageTitle from "../../globalComponents/__elements/PageTitle/PageTitle";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

class ArtistView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Fragment>
        <PageTitle pageTitle="Test" />

        <div className="container">
          <div className="row">
            <div className="col-6 artist" />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ArtistView;
