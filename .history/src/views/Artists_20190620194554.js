import React from "react";

const ArtistsView = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-6 offset-sm-3">
          <form action="">
            <div className="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                placeholder="Enter email"
              />
            </div>
          </form>
        </div>
      </div>
      <h1>Artists</h1>
    </div>
  );
};

export default ArtistsView;
