import React from "react";
import SearchForm from "../components/Form/Form";

const ArtistsView = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-6 offset-sm-3">
          <SearchForm />
        </div>
      </div>
      <h1>Artists</h1>
    </div>
  );
};

export default ArtistsView;
