import React, { Component } from "react";
import SearchForm from "../components/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "",
      songs: [],
      artists: []
    };
  }

  handleChange = e => {
    this.setState({ searchTerm: e.target.value });
  };

  escapeJSON = json => {
    exports.escapeJSON = function(json) {
      const escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
      const meta = {
        // table of character substitutions
        "\b": "\\b",
        "\t": "\\t",
        "\n": "\\n",
        "\f": "\\f",
        "\r": "\\r",
        '"': '\\"',
        "\\": "\\\\"
      };

      escapable.lastIndex = 0;
      return escapable.test(json)
        ? '"' +
            json.replace(escapable, function(a) {
              let c = meta[a];
              return typeof c === "string"
                ? c
                : "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4);
            }) +
            '"'
        : '"' + json + '"';
    };
  };

  fetchSongs = async url => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);
      let escapedResponse = await this.escapeJSON(response);
      let data = await escapedResponse.json();

      this.setState({
        songs: [...this.state.songs, data.songs],
        artists: [...this.state.songs, data.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  componentDidUpdate() {
    console.log(this.state.artists);
  }

  render() {
    let { searchTerm, artists } = this.state;

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Artists</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <SearchForm
              searchTerm={searchTerm}
              handleChange={this.handleChange}
            />

            <SearchResults artists={artists} />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
