import React, { Fragment } from "react";
import PageTitle from "../components/shared/PageTitle/PageTitle";

const ArtistView = () => {
  const pageTitle = "Henkie";
  return (
    <Fragment>
      <PageTitle pageTitle={pageTitle} />

      <div className="row">
        <div className="col-6" />
      </div>
    </Fragment>
  );
};

export default ArtistView;
