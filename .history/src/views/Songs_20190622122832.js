import React, { Component } from "react";

class Songs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      songs: []
    };
  }

  // Async call to fetch the data for displaying artists
  fetchSongs = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      // Put the fetched data into the artists state array, using spread. This it is a non-destructive approach of assigning data into state.
      this.setState({
        songs: [...this.state.songs, ...data]
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return <div />;
  }
}

export default Songs;
