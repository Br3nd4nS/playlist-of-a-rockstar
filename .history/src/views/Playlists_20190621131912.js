import React, { Component } from "react";
import PageTitle from "../components/Global/PageTitle/PageTitle";
import Button from "../components/Button/Button";

class PlaylistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playlists: []
    };

    this.buttonProps = {
      text: "Add new playlist",
      type: "button",
      className: "btn btn-primary",
      dataToggle: "modal",
      dataTarget: "#exampleModal"
    };
  }

  addNewPlaylist = e => {};

  render() {
    const title = "Playlists";

    return (
      <div className="container">
        <PageTitle title={title} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Button props={this.buttonProps} />
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
