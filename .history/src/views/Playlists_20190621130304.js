import React, { Component } from "react";

class PlaylistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playlists: []
    };
  }

  addNewPlaylist = () => {};

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Playlists</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <SearchForm
              searchTerm={searchTerm}
              handleChange={this.handleChange}
              filteredArtists={this.filteredArtists}
            />

            <SearchResults
              artists={artists}
              filteredArtists={filteredArtists}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
