import React, { Component } from "react";
import PageTitle from "../components/Global/PageTitle/PageTitle";
import Button from "../components/Button/Button";

import Form from "../components/Form/Form";

class PlaylistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      playlists: []
    };

    this.pageTitle = "Playlists";

    this.formProps = {
      formPurpose: "create-playlist",
      formPlaceholder: "Naam van nieuwe playlist"
    };

    this.buttonProps = {
      text: "Add new playlist",
      type: "button",
      iconClassName: "fas fa-forward",
      className: "btn btn--yellow"
    };
  }

  handleChange = e => {
    let inputValue = e.target.value;

    this.setState({
      inputValue
    });

    console.log(inputValue);
  };

  addNewPlaylist = e => {};

  render() {
    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form handleChange={this.handleChange} formProps={this.formProps} />

            <Button props={this.buttonProps} />
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
