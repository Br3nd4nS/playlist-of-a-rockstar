import React, { Component } from "react";
import Form from "../components/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";
import PageTitle from "../components/Global/PageTitle/PageTitle";
import _ from "lodash";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      songs: [],
      artists: [],
      filteredArtists: null
    };

    this.formProps = {
      formPurpose: "search-artist",
      formPlaceholder: "Zoek je favoriete artiest"
    };
  }

  debounceInput() {
    _.debounce(() => {
      this.searchDebounced = this.state.searchTerm;
    }, 250);
  }

  handleChange = e => {
    let searchValue = e.target.value;

    let currentArtistList = [];
    let updatedArtistsList = [];

    if (searchValue !== "") {
      currentArtistList = this.state.artists;

      // Use .filter() to determine which items should be displayed
      // based on the search terms
      updatedArtistsList = currentArtistList.filter(artist => {
        const name = artist.name.toLowerCase();

        // change search term to lowercase
        const inputValue = searchValue.toLowerCase();
        // check to see if the current list item includes the search term
        // If it does, it will be added to newList. Using lowercase eliminates
        // issues with capitalization in search terms and search content
        return name.includes(inputValue);
      });
    } else {
      updatedArtistsList = this.state.artists;
    }

    this.setState({
      inputValue: searchValue,
      filteredArtists: updatedArtistsList
    });
  };

  fetchSongs = async url => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);
      let data = await response.json();

      this.setState({
        songs: [...this.state.songs, ...data.songs],
        artists: [...this.state.artists, ...data.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      filteredArtists: nextProps.artists
    });
  }

  componentDidUpdate() {}

  render() {
    const title = "Artists";
    let { inputValue, artists, filteredArtists } = this.state;

    return (
      <div className="container">
        <PageTitle title={title} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form
              formProps={this.formProps}
              inputValue={inputValue}
              handleChange={this.handleChange}
              filteredArtists={this.filteredArtists}
            />

            <SearchResults
              artists={artists}
              filteredArtists={filteredArtists}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
