import React, { Component } from "react";
import PageTitle from "../components/shared/PageTitle/PageTitle";
import Button from "../components/shared/Button/Button";
import Form from "../components/shared/Form/Form";
import SavedPlaylists from "../components/SavedPlaylists/SavedPlaylists";

class PlaylistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      playlists: [],
      counter: 0
    };

    this.pageTitle = "Playlists";

    this.formProps = {
      formPurpose: "create-playlist",
      formPlaceholder: "Naam van nieuwe playlist"
    };

    this.buttonProps = {
      text: "Add new playlist",
      type: "button",
      iconClassName: "fas fa-forward",
      className: "btn btn--yellow"
    };
  }

  createID = () => {
    this.setState({
      counter: this.state.counter + 1
    });
    return this.state.counter;
  };

  addPlaylist = e => {
    const playlistName = this.state.inputValue;
    const newPlaylist = {
      name: playlistName,
      id: this.createID(),
      songs: []
    };

    this.setState({
      playlists: [...this.state.playlists, newPlaylist]
    });
  };

  handleChange = e => {
    let inputValue = e.target.value;

    this.setState({
      inputValue
    });
  };

  deletePlaylist = id => {
    // Filter the 'playlist' which need to be deleted out of the playlists array
    const playlists = this.state.playlists.filter(playlist => {
      return playlist.id !== id;
    });

    this.setState({ playlists });
  };

  render() {
    const { playlists } = this.state;
    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form handleChange={this.handleChange} formProps={this.formProps} />

            <Button
              props={this.buttonProps}
              addPlaylist={this.props.addPlaylist}
            />

            <SavedPlaylists
              playlists={playlists}
              deletePlaylist={this.deletePlaylist}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
