import React, { Component } from "react";

class Songs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: "",
      songs: [],
      filteredSongs: []
    };
  }

  // Async call to fetch the data for displaying artists
  fetchSongs = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      // Put the fetched data into the artists state array, using spread. This it is a non-destructive approach of assigning data into state.
      this.setState({
        songs: [...this.state.songs, ...data]
      });
    } catch (error) {
      console.log(error);
    }
  };

  handleChange = e => {
    let searchValue = e.target.value;

    let currentArtists = [];
    let updatedArtists = [];

    if (searchValue !== "") {
      currentArtists = this.state.artists;

      updatedArtists = currentArtists.filter(artist => {
        const artistName = artist.name.toLowerCase();

        // change search term to lowercase
        const inputValue = searchValue.toLowerCase();
        // check to see if the current list item includes the search term
        // If it does, it will be added to newList. Using lowercase eliminates
        // issues with capitalization in search terms and search content
        return artistName.includes(inputValue);
      });
    } else {
      updatedArtists = this.state.artists;
    }

    this.setState({
      inputValue: searchValue,
      filteredArtists: updatedArtists
    });
  };

  componentDidMount() {
    this.fetchSongs("http://localhost:3000/songs");
  }

  render() {
    return <div />;
  }
}

export default Songs;
