import React, { Component } from "react";
import Form from "../../globalComponents/shared/Form/Form";
import SearchResults from "../../globalComponents/SearchResults/SearchResults";
import PageTitle from "../../globalComponents/shared/PageTitle/PageTitle";
import _ from "lodash";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      artists: this.props.artists,
      filteredArtists: []
    };

    this.formProps = {
      formPurpose: "search-artist",
      formPlaceholder: "Zoek je favoriete artiest"
    };

    this.pageTitle = "Artists";
  }

  // Use debounce tot preserve performance.
  debounceInput() {
    _.debounce(() => {
      this.searchDebounced = this.state.searchTerm;
    }, 250);
  }

  handleChange = e => {
    let searchValue = e.target.value;

    let currentArtists = [];
    let updatedArtists = [];

    if (searchValue !== "") {
      currentArtists = this.props.artists;

      updatedArtists = currentArtists.filter(artist => {
        // put all letters in artist array to lowercase letters
        const artistName = artist.name.toLowerCase();

        // put all letters in searchterm to lowercase letters
        const inputValue = searchValue.toLowerCase();

        // check if the artist array has something which is identical to the inputValue and put this into updatedArtists
        return artistName.includes(inputValue);
      });
    } else {
      updatedArtists = this.props.artists;
    }

    this.setState({
      inputValue: searchValue,
      filteredArtists: updatedArtists
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.artists !== this.props.artists) {
      this.setState({
        filteredArtists: this.props.artists
      });
    }
  }

  componentDidMount() {
    this.props.fetchData("http://localhost:3000/artists");
    console.log(this.props);
  }

  render() {
    let { inputValue, artists, filteredArtists } = this.state;

    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form
              formProps={this.formProps}
              inputValue={inputValue}
              handleChange={this.handleChange}
              filteredArtists={this.filteredArtists}
            />

            <SearchResults
              path={this.props.match}
              artists={artists}
              filteredResults={filteredArtists}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
