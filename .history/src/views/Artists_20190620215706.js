import React, { Component } from "react";
import SearchForm from "../components/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "",
      songs: [],
      artists: []
    };
  }

  handleChange = e => {
    this.setState({ searchTerm: e.target.value });
  };

  fetchSongs = async url => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);

      console.log(response);
      let data = await response.json();
      console.log(data);

      this.setState({
        songs: [...this.state.songs, data.songs],
        artists: [...this.state.songs, data.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  componentDidUpdate() {
    console.log(this.state.artists);
  }

  render() {
    let { searchTerm, artists } = this.state;

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Artists</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <SearchForm
              searchTerm={searchTerm}
              handleChange={this.handleChange}
            />

            <SearchResults artists={artists} />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
