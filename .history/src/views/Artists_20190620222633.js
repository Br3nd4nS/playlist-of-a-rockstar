import React, { Component } from "react";
import SearchForm from "../components/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "",
      songs: [],
      artists: []
    };
  }

  handleChange = e => {
    let inputValue = e.target.value;
    this.setState({ searchTerm: inputValue });
  };

  filteredResults = e => {
    let inputValue = e.target.value;
    return this.state.artists.filter(artist => {
      console.log(artist);
      return artist.name === inputValue;
    });
  };

  fetchSongs = async url => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);
      let data = await response.json();
      let test2 = await JSON.parse(JSON.stringify(data));

      this.setState({
        songs: [...this.state.songs, test2.songs],
        artists: [...this.state.songs, test2.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  componentDidUpdate() {}

  render() {
    let { searchTerm, artists } = this.state;

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Artists</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <SearchForm
              searchTerm={searchTerm}
              handleChange={this.handleChange}
              filteredResults={this.filteredResults}
            />

            <SearchResults artists={artists} />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
