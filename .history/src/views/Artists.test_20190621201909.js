import fetchSongs from "./Artists";

test("the data is peanut butter", async () => {
  const data = await fetchSongs();
  expect(data).toBe(Object);
});

test("the fetch fails with an error", async () => {
  expect.assertions(1);
  try {
    await fetchSongs();
  } catch (e) {
    expect(e).toMatch("error");
  }
});
