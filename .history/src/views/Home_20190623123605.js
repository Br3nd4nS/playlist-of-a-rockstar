import React from "react";
import { BrowserRouter as Link } from "react-router-dom";

import routes from "../routing/routes";

const HomeView = () => {
  const pageBlocks = routes.map(({ path, view }, key) => {
    return path !== "/" ? (
      <div className="col-4" key={key}>
        <div className="pageBlock">
          <Link
            to={path}
            activeClassName="isActive txt--color-white"
            className="pageBlock__link"
          >
            <i class="fas fa-music" />

            {view}
          </Link>
        </div>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
