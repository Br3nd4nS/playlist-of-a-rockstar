import React from "react";

const HomeView = ({ pageBlocks }) => {
  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
