import React, { Component } from "react";

import Form from "../../globalComponents/shared/Form/Form";
import SearchResults from "../../globalComponents/SearchResults/SearchResults";
import PageTitle from "../../globalComponents/shared/PageTitle/PageTitle";

class Songs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
      filteredSongs: []
    };

    this.formProps = {
      formPurpose: "search-song",
      formPlaceholder: "Zoek je favoriete nummer"
    };

    this.pageTitle = "Songs";
  }

  handleChange = e => {
    let searchValue = e.target.value;

    let currentSongs = [];
    let updatedSongs = [];

    if (searchValue !== "") {
      currentSongs = this.props.songs;

      updatedSongs = currentSongs.filter(song => {
        // put all letters in songs array to lowercase letters
        const songName = song.name.toLowerCase();

        // put all letters in searchterm to lowercase letters
        const inputValue = searchValue.toLowerCase();

        return songName.includes(inputValue);
      });
    } else {
      updatedSongs = this.props.songs;
    }

    this.setState({
      inputValue: searchValue,
      filteredSongs: updatedSongs
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.songs !== this.props.songs) {
      console.log("update triggered");
      this.setState({
        filteredSongs: this.props.songs
      });
    }
  }

  componentDidMount() {
    this.props.fetchData("http://localhost:3000/songs");
  }

  render() {
    let { inputValue, songs, filteredSongs } = this.state;

    return (
      <div className="container">
        <PageTitle title={this.pageTitle} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <Form
              formProps={this.formProps}
              inputValue={inputValue}
              handleChange={this.handleChange}
              filteredResults={this.filteredSongs}
            />

            <SearchResults
              path={this.props.match}
              songs={songs}
              filteredResults={filteredSongs}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Songs;
