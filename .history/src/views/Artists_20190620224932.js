import React, { Component } from "react";
import SearchForm from "../components/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "",
      songs: [],
      artists: []
    };
  }

  handleChange = e => {
    let inputValue = e.target.value;
    this.setState({ searchTerm: inputValue });
  };

  filteredResults = e => {
    let inputValue = e.target.value;
    console.log(inputValue);
    return this.state.artists.filter(artist => {
      return artist.name === inputValue.toLowerCase();
    });
  };

  fetchSongs = async (url, cb) => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);
      let data = await response;
      // let test2 = await JSON.parse(JSON.stringify(data));

      this.setState({
        songs: [...this.state.songs, data.songs],
        artists: [...this.state.artists, data.artists]
      });

      cb(this.state.artists);
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs(
      "https://www.teamrockstars.nl/sites/default/files/db.json",
      artists => {
        artists.map(artist => {
          return console.log("artiest " + artist.name);
        });
      }
    );
  }

  componentDidUpdate() {}

  render() {
    let { searchTerm, artists } = this.state;

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Artists</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <SearchForm
              searchTerm={searchTerm}
              handleChange={this.handleChange}
              filteredResults={this.filteredResults}
            />

            <SearchResults artists={artists} />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
