import fetchSongs from "./Artists";

test("the data is an json object", async () => {
  const data = await fetchSongs(
    "https://www.teamrockstars.nl/sites/default/files/db.json"
  );
  expect(data).toBe(Object);
});

test("the fetch fails with an error", async () => {
  expect.assertions(1);
  try {
    await fetchSongs();
  } catch (e) {
    expect(e).toMatch("error");
  }
});
