import React from "react";
import { BrowserRouter as Link, Route, NavLink } from "react-router-dom";

import routes from "../../routing/routes";

import Tile from "../../globalComponents/__elements/Tile/Tile";

const HomeView = () => {
  const pageBlocks = routes.map(({ path, view, icon }, key) => {
    return path !== "/" && path !== "/artists/artist" ? (
      <div className="col-sm-4 mt-4 mb-4" key={key} match>
        <NavLink to={path} className="pageblock">
          <Tile icon={icon} view={view} />
        </NavLink>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
