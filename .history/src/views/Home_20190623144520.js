import React from "react";
import { BrowserRouter as NavLink } from "react-router-dom";

import routes from "../routing/routes";

const HomeView = () => {
  const pageBlocks = routes.map(({ path, view }, key) => {
    return path !== "/" ? (
      <div className="col-4" key={key}>
        <div className="pageBlock">
          <NavLink
            to={path}
            activeClassName="isActive txt--color-white"
            className="pageBlock__link"
          >
            <div className="pageBlock">
              <i className="fas fa-music" />

              {view}
            </div>
          </NavLink>
        </div>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>
    </div>
  );
};

export default HomeView;
