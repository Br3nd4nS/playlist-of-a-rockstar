import React, { Component } from "react";
import PageTitle from "../components/Global/PageTitle/PageTitle";

class PlaylistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playlists: []
    };
  }

  addNewPlaylist = () => {};

  render() {
    const title = "Playlists";

    return (
      <div className="container">
        <PageTitle title={title} />

        <div className="row">
          <div className="col-sm-6 offset-sm-3" />
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
