import React from "react";
import { BrowserRouter as Link, Route, NavLink } from "react-router-dom";

import ArtistsView from "./Artists";
import PlaylistsView from "./Playlists";
import SongsView from "./Songs";

import routes from "../routing/routes";

const HomeView = ({ artists, songs, playlists }) => {
  const pageBlocks = routes.map(({ path, view }, key) => {
    return path !== "/" ? (
      <div className="col-4" key={key}>
        <div className="pageBlock">
          <NavLink
            to={path}
            activeClassName="isActive txt--color-white"
            className="pageBlock__link"
          >
            <i className="fas fa-music" />

            {view}
          </NavLink>
        </div>
      </div>
    ) : null;
  });

  return (
    <div className="container">
      <div className="row">{pageBlocks}</div>

      {/* <Route
        path="/artists"
        render={() => (
          <ArtistsView fetchData={this.fetchData} artists={artists} />
        )}
      />

      <Route
        path="/songs"
        render={() => <SongsView fetchData={this.fetchData} songs={songs} />}
      />

      <Route
        path="/playlists"
        render={() => <PlaylistsView playlists={playlists} />}
      /> */}
    </div>
  );
};

export default HomeView;
