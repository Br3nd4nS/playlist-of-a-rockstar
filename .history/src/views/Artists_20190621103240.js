import React, { Component } from "react";
import SearchForm from "../components/Form/Form";
import SearchResults from "../components/SearchResults/SearchResults";
import { debounce } from "lodash";

class ArtistsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "",
      songs: [],
      artists: []
    };

    this.debouncedSearch = debounce(this.debouncedSearch, 250);
  }

  handleChange = e => {
    let inputValue = e.target.value;
    this.setState({ searchTerm: this.debouncedSearch(inputValue) });
  };

  filteredResults = e => {
    const filteredArtists = this.state.artists;
    let inputValue = e.target.value;

    const test = filteredArtists.filter(artist => {
      return artist.name.toLowerCase() === inputValue.toLowerCase();
    });
    console.log(test);
  };

  fetchSongs = async (url, cb) => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);
      let data = await response.json();

      this.setState({
        songs: [...this.state.songs, ...data.songs],
        artists: [...this.state.artists, ...data.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  componentDidUpdate() {}

  render() {
    let { searchTerm, artists } = this.state;

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Artists</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            <SearchForm
              searchTerm={searchTerm}
              handleChange={this.handleChange}
              filteredResults={this.filteredResults}
            />

            <SearchResults
              artists={artists}
              filteredResults={this.filteredResults}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ArtistsView;
