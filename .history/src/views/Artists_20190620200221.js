import React from "react";
import SearchForm from "../components/Form/Form";

const ArtistsView = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h1>Artists</h1>
        </div>
      </div>

      <div className="row">
        <div className="col-sm-6 offset-sm-3">
          <SearchForm />
        </div>
      </div>
    </div>
  );
};

export default ArtistsView;
