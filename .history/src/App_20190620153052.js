import React, { Component } from "react";
import { BrowserRouter as Route, NavLink } from "react-router-dom";

import routes from "./routing/routes";

import "./AppStyles.scss";
import "./NavStyles.scss";
import "./utilities.scss";
import Header from "./components/Global/Header/Header";

class App extends Component {
  render() {
    const viewComponents = routes.map(({ url, component }, key) => (
      <Route exact path={url} component={component} key={key} />
    ));

    const navLinks = routes.map(({ url, view }, key) => (
      <li className="main-nav__item">
        <NavLink
          to={url}
          activeClassName="isActive"
          className="main-nav__link"
          key={key}
        >
          {view}
        </NavLink>
      </li>
    ));
    return (
      <div>
        {navLinks}
        <Header navLinks={navLinks} />

        {viewComponents}
      </div>
    );
  }
}

export default App;
