import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";

import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

import routes from "./routing/routes";

import "./AppStyles.scss";
import "./NavStyles.scss";
import "./utilities.scss";
import Navigation from "./components/Global/Navigation/Navigation";
import Header from "./components/Global/Header/Header";

class App extends Component {
  render() {
    const viewComponents = routes.map(({ url, component }, key) => (
      <Route exact path={url} component={component} key={key} />
    ));
    return (
      <BrowserRouter>
        <ul className="main-nav flex flex--divide-h flex--center-v">
          <li className="main-nav__item">
            <NavLink
              to="/"
              activeClassName="isActive"
              className="main-nav__link"
            >
              Home
            </NavLink>
          </li>

          <li className="main-nav__item">
            <NavLink
              to="/artists"
              activeClassName="isActive"
              className="main-nav__link"
            >
              Artists
            </NavLink>
          </li>

          <li className="main-nav__item">
            <NavLink
              to="/playlists"
              activeClassName="isActive"
              className="main-nav__link"
            >
              My Playlists
            </NavLink>
          </li>
        </ul>
        <Header />

        {viewComponents}
      </BrowserRouter>
    );
  }
}

export default App;
