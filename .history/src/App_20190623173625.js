import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import ArtistsView from "./views/Artists";
import ArtistView from "./views/Artist";
import PlaylistsView from "./views/Playlists";
import SongsView from "./views/Songs";
import HomeView from "./views/Home";

import routes from "./routing/routes";

import "./AppStyling.scss";
import "./NavStyling.scss";
import "./globalStyles/utilities.scss";
import Header from "./components/shared/Header/Header";
import Main from "./components/shared/Main/Main";

class App extends Component {
  constructor() {
    super();

    this.state = {
      artists: [],
      songs: [],
      playlists: []
    };
  }

  fetchData = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      url === "http://localhost:3000/artists"
        ? this.setState({
            artists: [...data]
          })
        : this.setState({
            songs: [...data]
          });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {}

  render() {
    console.log(this.state);
    const { artists, songs, playlists } = this.state;

    // Looping through routes to display all navigation links
    const navLink = routes.map(({ path, view }, key) => (
      <li className="main-nav__item" key={key}>
        <NavLink
          to={path}
          activeClassName="isActive"
          className="main-nav__link"
        >
          {view}
        </NavLink>
      </li>
    ));

    return (
      <Router>
        <Header navLinks={navLink} />

        <main className="App-main pt-5">
          <Route exact path="/home" render={() => <HomeView />} />
          <Route
            path="/artists"
            render={props => (
              <ArtistsView
                fetchData={this.fetchData}
                artists={artists}
                {...props}
              />
            )}
          />
          <Route path="/artists" render={props => <ArtistView {...props} />} />
          <Route
            path="/songs"
            render={props => (
              <SongsView fetchData={this.fetchData} songs={songs} {...props} />
            )}
          />
          <Route
            path="/playlists"
            render={props => <PlaylistsView playlists={playlists} {...props} />}
          />
        </main>
      </Router>
    );
  }
}

export default App;
