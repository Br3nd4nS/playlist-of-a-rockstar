import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import routes from "./routing/routes";

import "./AppStyling.scss";
import "./NavStyling.scss";
import "./globalStyles/utilities.scss";
import Header from "./components/shared/Header/Header";
import Main from "./components/shared/Main/Main";

class App extends Component {
  constructor() {
    super();

    this.state = {
      artists: [],
      songs: [],
      playlists: []
    };
  }

  fetchData = async (url, stateArray) => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      // Put the fetched data into the artists state array, using spread. This it is a non-destructive approach of assigning data into state.
      this.setState({
        stateArray: [...this.state.stateArray, ...data]
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const viewComponents = routes.map(({ url, component }, key) => (
      <Route
        exact
        path={url}
        fetchData={this.fetchData}
        component={component}
        key={key}
      />
    ));

    // Looping through routes to display all navigation links
    const navLinks = routes.map(({ url, view }, key) => (
      <li className="main-nav__item" key={key}>
        <NavLink
          to={url}
          activeClassName="isActive txt--color-white"
          className="main-nav__link"
        >
          {view}
        </NavLink>
      </li>
    ));

    return (
      <Router>
        <Header navLinks={navLinks} />

        <main className="App-main pt-5">
          <Switch>
            <main className="App-main pt-5">{viewComponents}</main>
          </Switch>
        </main>

        {/* <Main viewComponents={viewComponents} /> */}
      </Router>
    );
  }
}

/* const App = () => {
  // Looping through routes to display all components
  const viewComponents = routes.map(({ url, component }, key) => (
    <Route exact path={url} component={component} render={(props) => {<Component {...props}}} key={key} />
  ));

  // Looping through routes to display all navigation links
  const navLinks = routes.map(({ url, view }, key) => (
    <li className="main-nav__item" key={key}>
      <NavLink
        to={url}
        activeClassName="isActive txt--color-white"
        className="main-nav__link"
      >
        {view}
      </NavLink>
    </li>
  ));

  return (
    // Splitting code logic into header and body for readability
    <Router>
      <Header navLinks={navLinks} />

      <Switch>
        <Route exact path={url} component={component} key={key} />

        <Main viewComponents={viewComponents} />
      </Switch>
    </Router>
  );
}; */

export default App;
