import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import routes from "./routing/routes";

import "./AppStyling.scss";
import "./NavStyling.scss";
import "./globalStyles/utilities.scss";
import Header from "./components/shared/Header/Header";
import Main from "./components/shared/Main/Main";

const App = () => {
  // Looping through routes to display all components
  const viewComponents = routes.map(({ url, component }, key) => (
    <Route exact path={url} component={component} key={key} />
  ));

  // Looping through routes to display all navigation links
  const navLinks = routes.map(({ url, view }, key) => (
    <li className="main-nav__item" key={key}>
      <NavLink
        to={url}
        activeClassName="isActive txt--color-white"
        className="main-nav__link"
      >
        {view}
      </NavLink>
    </li>
  ));

  return (
    <Router>
      <Header navLinks={navLinks} />

      <Switch>
        <Main viewComponents={viewComponents} pageBlocks={pageBlocks} />
      </Switch>
    </Router>
  );
};

export default App;
