import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";

import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

import routes from "./routing/routes";

import "./AppStyles.scss";
import "./NavStyles.scss";
import "./utilities.scss";
import Navigation from "./components/Global/Navigation/Navigation";
import Header from "./components/Global/Header/Header";

class App extends Component {
  render() {
    const viewComponents = routes.map(({ url, component }, key) => (
      <Route exact path={url} component={component} key={key} />
    ));

    const navLinks = routes.map(({ url, component, view }, key) => (
      <li className="main-nav__item">
        <NavLink
          to={url}
          activeClassName="isActive"
          className="main-nav__link"
          key={key}
        >
          {view}
        </NavLink>
      </li>
    ));
    return (
      {navLinks}
      <BrowserRouter>
        
        <Header />

        {viewComponents}
      </BrowserRouter>
    );
  }
}

export default App;
