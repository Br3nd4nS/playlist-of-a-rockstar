import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";

import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

import routes from "./routing/routes";

import logo from "./logo.png";
import "./AppStyles.scss";
import "./NavStyles.scss";
import "./utilities.scss";
import Navigation from "./components/Global/Navigation/Navigation";

class App extends Component {
  render() {
    const viewComponents = routes.map(({ url, component }, key) => (
      <Route exact path={url} component={component} key={key} />
    ));
    return (
      <BrowserRouter>
        <Header />

        <header className="App-header">
          <div className="container">
            <div className="row">
              <div className="col-sm-4">
                <Navigation />
              </div>
            </div>
          </div>
        </header>
        {viewComponents}
      </BrowserRouter>
    );
  }
}

export default App;
