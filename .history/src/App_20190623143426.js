import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";
import SongsView from "./views/Songs";
import HomeView from "./views/Home";

import routes from "./routing/routes";

import "./AppStyling.scss";
import "./NavStyling.scss";
import "./globalStyles/utilities.scss";
import Header from "./components/shared/Header/Header";
import Main from "./components/shared/Main/Main";

class App extends Component {
  constructor() {
    super();

    this.state = {
      artists: [],
      songs: [],
      playlists: []
    };
  }

  fetchData = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      // Put the fetched data into the artists state array, using spread. This it is a non-destructive approach of assigning data into state.
      this.setState({
        artists: [...this.state.artists, ...data],
        songs: [...this.state.songs, ...data]
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { artists, songs, playlists } = this.state;

    // Looping through routes to display all navigation links
    const navLink = routes.map(({ path, view }, key) => (
      <li className="main-nav__item" key={key}>
        <NavLink
          to={path}
          activeClassName="isActive txt--color-white"
          className="main-nav__link"
        >
          {view}
        </NavLink>
      </li>
    ));

    return (
      <Router>
        <Header navLinks={navLink} />

        <Route exact path="/" render={() => <HomeView />} />
        <Route
          path="/artists"
          render={() => (
            <ArtistsView fetchData={this.fetchData} artists={artists} />
          )}
        />
        <Route
          path="/songs"
          render={() => <SongsView fetchData={this.fetchData} songs={songs} />}
        />
        <Route
          path="/playlists"
          render={() => <PlaylistsView playlists={playlists} />}
        />

        <Main />
      </Router>
    );
  }
}

export default App;
