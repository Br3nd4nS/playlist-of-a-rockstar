import React from "react";

const SavedPlaylists = ({ playlists, deletePlaylist }) => {
  const displayPlaylists = playlists.map((playlist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <h4>{playlist.name}</h4>{" "}
        <div>
          <button
            className="mr-2 ml-2 delete-btn"
            onClick={() => {
              deletePlaylist(playlist.id);
            }}
          >
            <i className="fa fa-times" />
          </button>{" "}
          <i className="fas fa-forward" />
        </div>
      </li>
    );
  });

  return <ul className="default-list p-5">{displayPlaylists}</ul>;
};

export default SavedPlaylists;
