import React from "react";

const SavedPlaylists = ({ playlists }) => {
  const displayPlaylists = playlists.map((playlist, key) => {
    console.info(playlists.length);
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <h4>{playlist.name}</h4> <i className="fas fa-forward" />
      </li>
    );
  });

  return <ul className="default-list p-5">{displayPlaylists}</ul>;
};

export default SavedPlaylists;
