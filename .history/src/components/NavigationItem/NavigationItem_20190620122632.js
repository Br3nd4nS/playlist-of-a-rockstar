import React, { Fragment } from "react";

import { BrowserRouter as NavLink } from "react-router-dom";
import routes from "../../routing/routes";

const NavigationItem = () => {
  // let key = 0;

  const navLinks = routes.map(route => {
    const generateKey = key => {
      return (key += key);
    };

    return (
      <Fragment>
        <li className="main-nav__item" key={generateKey(1)}>
          <NavLink
            to={route.url}
            activeClassName="isActive"
            className="main-nav__link"
          >
            {route.view}
          </NavLink>
        </li>
      </Fragment>
    );
  });

  return <div>{navLinks}</div>;
};

export default NavigationItem;
