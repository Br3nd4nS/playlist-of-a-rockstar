import React from "react";

import { BrowserRouter as NavLink } from "react-router-dom";
import routes from "../../routing/routes";

const NavigationItem = () => {
  const navLinks = routes.map(route => {
    return (
      <li className="main-nav__item">
        <NavLink
          to={route.url}
          activeClassName="isActive"
          className="main-nav__link"
        >
          {route.view}
        </NavLink>
      </li>
    );
  });

  return { navLinks };
};

export default NavigationItem;
