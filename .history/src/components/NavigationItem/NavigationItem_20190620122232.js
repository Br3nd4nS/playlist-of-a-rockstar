import React from "react";

import { BrowserRouter as NavLink } from "react-router-dom";
import routes from "../../routing/routes";

const NavigationItem = () => {
  // let key = 0;
  const generateKey = key => {
    return key.key++;
  };

  const navLinks = routes.map((route, key = 0) => {
    return (
      <li className="main-nav__item" key={generateKey(key)}>
        <NavLink
          to={route.url}
          activeClassName="isActive"
          className="main-nav__link"
        >
          {route.view}
        </NavLink>
      </li>
    );
  });

  return <div>{navLinks}</div>;
};

export default NavigationItem;
