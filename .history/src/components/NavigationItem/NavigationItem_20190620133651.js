import React, { Fragment } from "react";

import { BrowserRouter as NavLink } from "react-router-dom";

import routes from "../../routing/routes";

const NavigationItem = () => {
  let key = 1;

  const generateKey = () => {
    return key++;
  };

  const navLinks = routes.map(route => {
    console.log(key);
    return (
      <Fragment>
        <li className="main-nav__item" key={generateKey().toString()}>
          <NavLink
            to={route.url}
            activeClassName="isActive"
            className="main-nav__link"
          >
            {route.view}
          </NavLink>
        </li>
      </Fragment>
    );
  });

  return <div>{navLinks}</div>;
};

export default NavigationItem;
