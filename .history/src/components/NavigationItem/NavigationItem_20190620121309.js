import React from "react";

import { BrowserRouter as NavLink } from "react-router-dom";
// import routes from "../../routing/routes";

const NavigationItem = () => {
  const routes = [
    { url: "/", view: "Home" },
    { url: "/artists", view: "Artists" },
    { url: "/playlists", view: "Playlists" }
  ];

  console.log(typeof routes);

  /* const navLinks = routes.map(route => {
    return (
      <li className="main-nav__item">
        <NavLink
          to={route.url}
          activeClassName="isActive"
          className="main-nav__link"
        >
          {route.view}
        </NavLink>
      </li>
    );
  }); */

  return <div>demo</div>;
};

export default NavigationItem;
