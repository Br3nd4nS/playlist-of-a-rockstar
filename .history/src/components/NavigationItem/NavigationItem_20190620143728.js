import React, { Fragment, Component } from "react";

import { BrowserRouter as NavLink } from "react-router-dom";

import routes from "../../routing/routes";

class NavigationItem extends Component {
  render() {
    let key = 0;

    const generateKey = () => {
      console.log(key);

      return key++;
    };

    const navLinks = routes.map(({ route }, key) => (
        console.log(route);
      <NavLink
        exact
        to={route.url}
        activeClassName="isActive"
        className="main-nav__link"
        key={key}
      >
        {route.view}
      </NavLink>
    ));
    return <Fragment>{navLinks}</Fragment>;
  }

  /* const navLinks = routes.map(route => {
    return (
      <Fragment>
        <li className="main-nav__item" key={generateKey()}>
          <NavLink
            to={route.url}
            activeClassName="isActive"
            className="main-nav__link"
          >
            {route.view}
          </NavLink>
        </li>
      </Fragment>
    );
  });

  return <div>{navLinks}</div>; */
}

export default NavigationItem;
