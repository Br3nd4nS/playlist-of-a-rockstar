import React from "react";

const FormInput = ({ searchTerm, handleChange }) => {
  return (
    <div className="form-group">
      <input
        type="text"
        name="search-input"
        value={searchTerm}
        onChange={e => {
          handleChange(e);
        }}
        className="form-control search-artist__input"
        id="searchArtist"
        aria-describedby="searchArtist"
        placeholder="Zoek je favoriete artiest"
      />
    </div>
  );
};

export default FormInput;
