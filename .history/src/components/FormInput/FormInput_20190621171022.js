import React from "react";

const FormInput = ({ searchTerm, handleChange, formProps }) => {
  return (
    <div className="form-group">
      <input
        type="text"
        name={formProps.formPurpose}
        value={searchTerm}
        onChange={e => {
          handleChange(e);
        }}
        className={formProps.formPurpose + "__input form-control"}
        id={formProps.formPurpose}
        aria-describedby={formProps.formPurpose}
        placeholder={formProps.formPlaceholder}
      />
    </div>
  );
};

export default FormInput;
