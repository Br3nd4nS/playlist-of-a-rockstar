import React from "react";

const FormInput = ({ searchTerm, handleChange, formProps }) => {
  return (
    <div className="form-group">
      <input
        type="text"
        name="search-input"
        value={searchTerm}
        onChange={e => {
          handleChange(e);
        }}
        className={formProps.formPurpose + "__input form-control"}
        id="searchArtist"
        aria-describedby="searchArtist"
        placeholder="Zoek je favoriete artiest"
      />
    </div>
  );
};

export default FormInput;
