import React from "react";

const Button = ({ props }) => {
  return (
    <button
      type={props.type}
      className={props.className}
      data-toggle={props.dataToggle}
      data-target={props.dataTarget}
    >
      {props.text}
    </button>
  );
};

export default Button;
