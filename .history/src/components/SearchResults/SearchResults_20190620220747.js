import React from "react";

const SearchResults = ({ artists }) => {
  const displayArtists = artists.map((artist, key) => {
    return (
      <li className="default-list__item" key={key}>
        <h4>hallo {artist.name}</h4>
      </li>
    );
  });

  return <ul className="default-list">{displayArtists}</ul>;
};

export default SearchResults;
