import React from "react";
import { BrowserRouter as Link } from "react-router-dom";

const SearchResults = ({ filteredArtists, match }) => {
  let person = find(match.params.id);

  const displayArtists = filteredArtists.map((artist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <Link to={`${match.url}/${id}`}>
          {find(id).name}
          <h4>{artist.name}</h4> <i className="fas fa-forward" />
        </Link>
      </li>
    );
  });

  return <ul className="default-list p-4">{displayArtists}</ul>;
};

export default SearchResults;
