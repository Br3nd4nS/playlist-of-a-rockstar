import React from "react";

import List from "../../components/shared/List/List";

const SearchResults = ({ filteredArtists }) => {
  return <List filteredArtists={filteredArtists} />;
};

export default SearchResults;
