import React from "react";

import ListItem from '../../../List/ListItem/ListItem'


const SearchResults = ({ filteredArtists }) => {
  const displayArtists = filteredArtists.map((artist, key) => {
    return (
      <ListItem artist={artist} key={key}>
    );
  });

  return <ul className="default-list p-4">{displayArtists}</ul>
};

export default SearchResults;
