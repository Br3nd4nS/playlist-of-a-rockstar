import React from "react";

import List from "../../components/shared/List/List";

const SearchResults = ({ artists, filteredArtists }) => {
  const displayArtists = filteredArtists.map((artist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <h4>{artist.name}</h4> <i className="fas fa-forward" />
      </li>
    );
  });

  return <List />;
};

export default SearchResults;
