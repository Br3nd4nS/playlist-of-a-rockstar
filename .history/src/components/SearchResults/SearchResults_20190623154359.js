import React from "react";

import ListItem from "../shared/List/ListItem/ListItem";
import "../shared/List/ListStyling.scss";

const SearchResults = ({ filteredArtists, props }) => {
  const displayArtists = filteredArtists.map((artist, key) => {
    return <ListItem artist={artist} key={key} props={props} />;
  });

  return <ul className="default-list p-4">{displayArtists}</ul>;
};

export default SearchResults;
