import React from "react";

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  NavLink
} from "react-router-dom";

const SearchResults = ({ filteredArtists, match }) => {
  const displayArtists = filteredArtists.map((artist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <Link to={match.url + "?name=" + artist.name}>
          <h4>{artist.name}</h4> <i className="fas fa-forward" />
        </Link>

        <Route
          exact
          path={match.path}
          render={() => <h3>Please select a topic.</h3>}
        />
      </li>
    );
  });

  return <ul className="default-list p-4">{displayArtists}</ul>;
};

export default SearchResults;
