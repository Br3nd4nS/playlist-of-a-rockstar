import React from "react";

const SearchResults = ({ artists, filteredArtists }) => {
  const displayArtists = filteredArtists.map((artist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <h4>{artist.name}</h4> <i className="fas fa-forward" />
      </li>
    );
  });

  if (filteredArtists.length === 0) {
    return <ul className="default-list p-5">{displayArtists}</ul>;
  } else {
    return null;
  }
};

export default SearchResults;
