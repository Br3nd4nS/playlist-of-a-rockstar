import React from "react";

const SearchResults = ({ artists, filteredResults }) => {
  const displayArtists = artists.map((artist, key) => {
    return (
      <li className="default-list__item pt-1 pb-1" key={key}>
        <h4>{artist.name}</h4>
      </li>
    );
  });

  return <ul className="default-list p-5">{displayArtists}</ul>;
};

export default SearchResults;
