import React from "react";

const SearchResults = ({ artists }) => {
  const displayArtists = artists.map((artist, key) => {
    return <li className="default-list__item" />;
  });

  return <ul className="default-list" />;
};

export default SearchResults;
