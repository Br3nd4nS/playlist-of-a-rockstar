import React from "react";

const SearchResults = ({ artists, filteredResults }) => {
  const displayArtists = filteredResults.map((artist, key) => {
    return (
      <li className="default-list__item" key={key}>
        <h4>{artist.name}</h4>
      </li>
    );
  });

  return <ul className="default-list">{displayArtists}</ul>;
};

export default SearchResults;
