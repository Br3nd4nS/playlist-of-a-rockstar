import React from "react";

const SearchResults = ({ artists, filteredResults }) => {
  const displayArtists = filteredResults().map((artist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <h4>{artist.name}</h4> <i class="fas fa-forward" />
      </li>
    );
  });

  return <ul className="default-list p-5">{displayArtists}</ul>;
};

export default SearchResults;
