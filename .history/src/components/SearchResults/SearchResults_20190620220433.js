import React from "react";

const SearchResults = ({ artists }) => {
  /* const test = () => {
      for(let artist of artists) {
        return (
            <li className="default-list__item" key={artist.id}>
              <h4>{artist.name}</h4>
            </li>
          );
      }
  }   */
  const displayArtists = artists.map((artist, key) => {
    console.info(artist.name);
    return (
      <li className="default-list__item" key={key}>
        <h4>{artist.name}</h4>
      </li>
    );
  });

  return <ul className="default-list">{displayArtists}</ul>;
};

export default SearchResults;
