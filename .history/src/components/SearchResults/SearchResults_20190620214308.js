import React from "react";

const SearchResults = ({ artists }) => {
  const displayArtists = artists.map((artist, key) => {
    console.log(artist.name);
    return (
      <li className="default-list__item" key={key}>
        <h4>{artist.name}</h4>
      </li>
    );
  });

  return <ul className="default-list">{displayArtists}</ul>;
};

export default SearchResults;
