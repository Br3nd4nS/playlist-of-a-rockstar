import React from "react";

const SearchForm = ({ handleChange, searchTerm, filteredResults }) => {
  return (
    <form className="search-form" action="">
      <div className="form-group">
        <label htmlFor="searchArtist">Email address</label>
        <input
          type="text"
          name="search-input"
          value={searchTerm}
          onChange={e => {
            handleChange(e, "value");
            filteredResults(e, "value");
          }}
          className="form-control"
          id="searchArtist"
          aria-describedby="searchArtist"
          placeholder="Zoek je favoriete artiest"
        />
      </div>
    </form>
  );
};

export default SearchForm;
