import React from "react";

import "./FormStyling.scss";

import FormInput from "../FormInput.FormInput";

const SearchForm = ({ handleChange, searchTerm }) => {
  return (
    <form className="search-form" action="">
      <FormInput />
    </form>
  );
};

export default SearchForm;

/* onChange={e => {
    handleChange(e);
    filteredArtists(e);
  }} */
