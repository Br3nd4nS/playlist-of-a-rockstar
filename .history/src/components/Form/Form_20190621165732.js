import React from "react";

import FormInput from "../FormInput/FormInput";
import "./FormStyling.scss";

const Form = ({ handleChange, searchTerm, formPurpose }) => {
  return (
    <form className={formPurpose} action="">
      <FormInput
        formPurpose={formPurpose}
        handleChange={handleChange}
        searchTerm={searchTerm}
      />
    </form>
  );
};

export default Form;
