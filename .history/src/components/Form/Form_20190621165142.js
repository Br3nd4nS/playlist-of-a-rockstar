import React from "react";

import FormInput from "../FormInput/FormInput";
import "./FormStyling.scss";

const Form = ({ handleChange, searchTerm }) => {
  return (
    <form className="search-artist" action="">
      <FormInput handleChange={handleChange} searchTerm={searchTerm} />
    </form>
  );
};

export default Form;
