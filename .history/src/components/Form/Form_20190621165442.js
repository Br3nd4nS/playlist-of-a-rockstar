import React from "react";

import FormInput from "../FormInput/FormInput";
import "./FormStyling.scss";

const Form = ({ handleChange, searchTerm }) => {
  const formPurpose = "search-artist";

  return (
    <form className={formPurpose} action="">
      <FormInput handleChange={handleChange} searchTerm={searchTerm} />
    </form>
  );
};

export default Form;
