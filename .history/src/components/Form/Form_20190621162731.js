import React from "react";

import FormInput from "../FormInput/FormInput";
import "./FormStyling.scss";

const SearchForm = ({ handleChange, searchTerm }) => {
  return (
    <form className="search-form" action="">
      <FormInput />
    </form>
  );
};

export default SearchForm;
