import React, { Component } from "react";

class SearchForm extends Component {
  constructor(props) {
    super();

    this.state = {
      value: ""
    };
  }

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  fetchSongs = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      this.setState({
        songs: [...this.state.songs, data.value]
      });

      console.log(this.state);
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  render() {
    let { value } = this.state;
    return (
      <form className="search-form" action="">
        <div className="form-group">
          <label htmlFor="searchArtist">Email address</label>
          <input
            type="text"
            name="search-input"
            value={value}
            className="form-control"
            id="searchArtist"
            aria-describedby="searchArtist"
            placeholder="Zoek je favoriete artiest"
          />
        </div>
      </form>
    );
  }
}

export default SearchForm;
