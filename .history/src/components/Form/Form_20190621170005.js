import React from "react";

import FormInput from "../FormInput/FormInput";
import "./FormStyling.scss";

const Form = ({ handleChange, searchTerm, formProps }) => {
  return (
    <form className={formProps.formPurpose} action="">
      <FormInput
        formPurpose={formProps}
        handleChange={handleChange}
        searchTerm={searchTerm}
      />
    </form>
  );
};

export default Form;
