import React, { Component } from "react";

class SearchForm extends Component {
  constructor(props) {
    super();

    this.state = {
      searchTerm: "",
      songs: [],
      artists: []
    };
  }

  handleChange = e => {
    this.setState({ value: e.target.value });
  };

  fetchSongs = async url => {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";

    try {
      let response = await fetch(proxyurl + url);
      let data = await response.json();

      this.setState({
        songs: [...this.state.songs, data.songs],
        artists: [...this.state.songs, data.artists]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }

  componentDidUpdate() {
    console.log(this.state);
  }

  render() {
    let { searchTerm } = this.state;
    return (
      <form className="search-form" action="">
        <div className="form-group">
          <label htmlFor="searchArtist">Email address</label>
          <input
            type="text"
            name="search-input"
            value={searchTerm}
            onChange={this.handleChange}
            className="form-control"
            id="searchArtist"
            aria-describedby="searchArtist"
            placeholder="Zoek je favoriete artiest"
          />
        </div>
      </form>
    );
  }
}

export default SearchForm;
