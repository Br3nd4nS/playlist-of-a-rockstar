import React, { Component } from "react";

class SearchForm extends Component {
  constructor(props) {
    super();

    this.state = {};
  }

  fetchSongs = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      this.setState({
        songs: [...this.state.songs, data.value]
      });
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.fetchSongs("https://www.teamrockstars.nl/sites/default/files/db.json");
  }
}

export default SearchForm;
