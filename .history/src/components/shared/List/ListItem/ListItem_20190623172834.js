import React, { Fragment } from "react";

import { BrowserRouter as Route, Link, NavLink } from "react-router-dom";

const ListItem = ({ artist, key, path }) => {
  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <NavLink
          to={`${path.path}?name=${artist.name}`}
          className="pageblock-wrapper"
        >
          {artist.name}
        </NavLink>
        <i className="fas fa-forward" />
      </li>
    </Fragment>
  );
};

export default ListItem;
