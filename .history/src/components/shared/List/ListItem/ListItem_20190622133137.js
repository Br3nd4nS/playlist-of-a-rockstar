import React from "react";

const ListItem = ({ artist, key }) => {
  return (
    <li
      className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
      key={key}
    >
      {artist.name} <i className="fas fa-forward" />
    </li>
  );
};

export default ListItem;
