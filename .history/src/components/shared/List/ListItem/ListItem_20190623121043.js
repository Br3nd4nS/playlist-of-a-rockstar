import React, { Fragment } from "react";

import { BrowserRouter as Route, Link } from "react-router-dom";

const ListItem = ({ artist, key, match }) => {
  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <Link to={`${match.url} ?name= ${artist.name}`}>{artist.name}</Link>

        <i className="fas fa-forward" />
      </li>

      <Route
        exact
        path={match.path}
        render={() => <h3>Please select a topic.</h3>}
      />
    </Fragment>
  );
};

export default ListItem;
