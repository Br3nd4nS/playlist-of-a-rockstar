import React, { Fragment } from "react";

import ArtistView from "../../../../views/Artist";

import { BrowserRouter as Route, Link, NavLink } from "react-router-dom";

const ListItem = ({ artist, key, path }) => {
  let url = `${path.url}?name=${artist.name}`;
  const urlTest = url.replace(/\s/g, "");

  console.log(urlTest);

  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <NavLink to={url} className="pageblock-wrapper">
          {artist.name}
        </NavLink>

        <Route path={url} component={ArtistView} />

        {/* <Route
          path={`${path.path}?name=${artist.name}`}
          render={props => <ArtistView {...props} />}
        /> */}
        <i className="fas fa-forward" />
      </li>
    </Fragment>
  );
};

export default ListItem;
