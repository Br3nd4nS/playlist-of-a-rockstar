import React from "react";

const List = ({ filteredArtists }) => {
  const listItems = filteredArtists.map((artist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <h5>{artist.name}</h5>
        <button>
          <i className="fa fa-times" />
        </button>
        <i className="fas fa-forward" />
      </li>
    );
  });

  return { listItems };
};

export default List;
