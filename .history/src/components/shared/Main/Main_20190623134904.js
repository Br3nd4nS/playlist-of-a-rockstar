import React from "react";

import ArtistsView from "../../../views/Artists";
import PlaylistsView from "../../../views/Playlists";
import SongsView from "../../../views/Songs";
import HomeView from "../../../views/Home";

import { BrowserRouter as Route, Switch } from "react-router-dom";

const Main = ({ viewComponents, props }) => {
  return (
    <main className="App-main pt-5">
      <Route exact path="/" render={() => <HomeView />} />
      <Route
        path="/artists"
        render={() => (
          <ArtistsView fetchData={this.fetchData} artists={props.artists} />
        )}
      />
      <Route
        path="/songs"
        render={() => (
          <SongsView fetchData={this.fetchData} songs={props.songs} />
        )}
      />
      <Route
        path="/playlists"
        render={() => <PlaylistsView playlists={props.playlists} />}
      />
    </main>
  );
};

export default Main;
