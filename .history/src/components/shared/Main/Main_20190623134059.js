import React from "react";

import ArtistsView from "../../../views/Artists";
import PlaylistsView from "../../../views/Playlists";
import SongsView from "../../../views/Songs";
import HomeView from "../../../views/Home";

import { BrowserRouter as Route, Switch } from "react-router-dom";

const Main = ({ viewComponents, props }) => {
  return <main className="App-main pt-5" />;
};

export default Main;
