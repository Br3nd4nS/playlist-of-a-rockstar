import React from "react";

import FormInput from "./FormInput/FormInput";
import "./FormStyling.scss";

const Form = ({ handleChange, inputValue, formProps }) => {
  return (
    <form className={formProps.formPurpose} action="">
      <FormInput
        formProps={formProps}
        handleChange={handleChange}
        inputValue={inputValue}
      />
    </form>
  );
};

export default Form;
