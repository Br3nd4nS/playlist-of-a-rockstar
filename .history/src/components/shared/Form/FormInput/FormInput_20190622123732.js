import React from "react";

const FormInput = ({ inputValue, handleChange, formProps }) => {
  return (
    <div className="form-group">
      <input
        required
        type="email"
        name={formProps.formPurpose}
        value={inputValue}
        onChange={e => {
          handleChange(e);
        }}
        className={formProps.formPurpose + "__input form-control"}
        id={formProps.formPurpose}
        aria-describedby={formProps.formPurpose}
        placeholder={formProps.formPlaceholder}
      />
    </div>
  );
};

export default FormInput;
