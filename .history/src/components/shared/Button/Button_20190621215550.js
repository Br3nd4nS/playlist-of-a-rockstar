import React from "react";
import "./ButtonStyling.scss";

const Button = ({ props, addPlaylist }) => {
  return (
    <button
      onClick={e => {
        addPlaylist(e);
      }}
      type={props.type}
      className={props.className}
    >
      {props.text} <i className={props.iconClassName} />
    </button>
  );
};

export default Button;
