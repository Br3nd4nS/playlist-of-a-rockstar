import React from "react";
import "./ButtonStyling.scss";

const Button = ({ props }) => {
  return (
    <button type={props.type} className={props.className}>
      {props.text} <i className={props.iconClassName} />
    </button>
  );
};

export default Button;
