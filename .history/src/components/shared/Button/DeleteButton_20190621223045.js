import React from "react";

const DeleteButton = ({ deletePlaylist, playlist }) => {
  return (
    <button
      className="mr-2 ml-2 delete-btn"
      onClick={() => {
        deletePlaylist(playlist.id);
      }}
    >
      {" "}
      <i className="fa fa-times" />
    </button>
  );
};

export default DeleteButton;
