import React from "react";
import "./ButtonStyling.scss";

const Button = ({ props, addNewPlaylist }) => {
  return (
    <button
      onClick={e => {
        addNewPlaylist(e);
      }}
      type={props.type}
      className={props.className}
    >
      {props.text} <i className={props.iconClassName} />
    </button>
  );
};

export default Button;
