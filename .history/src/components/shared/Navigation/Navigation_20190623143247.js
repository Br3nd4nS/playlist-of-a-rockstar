import React from "react";

import routes from "../../../routing/routes";
import { BrowserRouter as NavLink, Router } from "react-router-dom";

import "./NavigationStyling.scss";

const Navigation = () => {
  const navLink = routes.map(({ path, view }, key) => (
    <li className="main-nav__item" key={key}>
      <NavLink
        to={path}
        activeClassName="isActive txt--color-white"
        className="main-nav__link"
      >
        {view}
      </NavLink>
    </li>
  ));

  return (
    <Router>
      <div className="col-8 col-sm-4">
        <nav>
          <ul className="main-nav flex flex--divide-h flex--center-v">
            {navLink}
          </ul>
        </nav>
      </div>
    </Router>
  );
};

export default Navigation;
