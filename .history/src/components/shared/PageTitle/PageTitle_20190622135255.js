import React from "react";

const PageTitle = ({ title }) => {
  return (
    <div className="row">
      <div className="col mb-4">
        <h2>{title}</h2>
      </div>
    </div>
  );
};

export default PageTitle;
