import React from "react";
import Navigation from "../Navigation/Navigation";
import Logo from "../Logo/Logo";

const Header = () => {
  return (
    <header className="App-header">
      <div className="container">
        <div className="row">
          <Navigation />
          <Logo />
        </div>
      </div>
    </header>
  );
};

export default Header;
