import React from "react";
import Navigation from "../Navigation/Navigation";

const Header = () => {
  return (
    <header>
      <div className="container">
        <div className="row">
          <Navigation />
          <Logo />
        </div>
      </div>
    </header>
  );
};

export default Header;
