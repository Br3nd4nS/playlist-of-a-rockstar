import React from "react";

import logo from "./logo.png";

const Logo = () => {
  return (
    <div className="col-sm-4 txt txt--c">
      <img src={logo} className="App-logo" alt="logo" />
    </div>
  );
};

export default Logo;
