import React from "react";

const Main = ({ viewComponents }) => {
  return <main className="App-main">{viewComponents}</main>;
};

export default Main;
