import React, { Component } from "react";

class Main extends Component {
  render() {
    return <main className="App-main pt-5">{this.props.viewComponents}</main>;
  }
}

export default Main;
