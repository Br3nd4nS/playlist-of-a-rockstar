import React from "react";

const Main = ({ viewComponents }) => {
  return <main className="App-main pt-2 pb-2">{viewComponents}</main>;
};

export default Main;
