import React, { Component } from "react";

class Main extends Component {
  render() {
    const { viewComponents } = this.props;

    return <main className="App-main pt-4">{viewComponents}</main>;
  }
}

export default Main;
