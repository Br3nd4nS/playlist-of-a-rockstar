import React from "react";

const Main = ({ viewComponents }) => {
  return <main className="App-main pt-10 pb-10">{viewComponents}</main>;
};

export default Main;
