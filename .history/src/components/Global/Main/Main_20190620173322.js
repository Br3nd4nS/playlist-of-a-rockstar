import React from "react";

const Main = ({ viewComponents }) => {
  return <main className="App-main pt-5 pb-5">{viewComponents}</main>;
};

export default Main;
