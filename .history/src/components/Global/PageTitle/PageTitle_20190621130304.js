import React from "react";

const PageHeader = () => {
  return (
    <div className="row">
      <div className="col">
        <h1>Playlists</h1>
      </div>
    </div>
  );
};

export default PageHeader;
