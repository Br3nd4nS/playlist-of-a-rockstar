import React from "react";

const PageTitle = ({ title }) => {
  return (
    <div className="row">
      <div className="col mb-2">
        <h1>{title}</h1>
      </div>
    </div>
  );
};

export default PageTitle;
