import React from "react";

const PageTitle = ({ title }) => {
  return (
    <div className="row">
      <div className="col">
        <h1>{title}</h1>
      </div>
    </div>
  );
};

export default PageTitle;
