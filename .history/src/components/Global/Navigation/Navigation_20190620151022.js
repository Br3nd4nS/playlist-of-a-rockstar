import React from "react";
import { BrowserRouter as NavLink } from "react-router-dom";

import routes from "../../../routing/routes";

const Navigation = () => {
  const navLinks = routes.map(({ url, view }, key) => (
    <li className="main-nav__item">
      <NavLink
        to={url}
        activeClassName="isActive"
        className="main-nav__link"
        key={key}
      >
        {view}
      </NavLink>
    </li>
  ));

  return (
    <div className="col-sm-4">
      <nav>
        <ul className="main-nav flex flex--divide-h flex--center-v">
          <li className="main-nav__item">
            <NavLink
              to="/"
              activeClassName="isActive"
              className="main-nav__link"
            >
              Home
            </NavLink>
          </li>

          <li className="main-nav__item">
            <NavLink
              to="/artists"
              activeClassName="isActive"
              className="main-nav__link"
            >
              Artists
            </NavLink>
          </li>

          <li className="main-nav__item">
            <NavLink
              to="/playlists"
              activeClassName="isActive"
              className="main-nav__link"
            >
              My Playlists
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navigation;
