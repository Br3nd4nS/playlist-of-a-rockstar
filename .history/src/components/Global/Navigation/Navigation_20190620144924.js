import React from "react";
import NavigationItem from "../../NavigationItem/NavigationItem";

const Navigation = () => {
  return (
    <div className="col-sm-4">
      <nav>
        <ul className="main-nav flex flex--divide-h flex--center-v" />
      </nav>
    </div>
  );
};

export default Navigation;
