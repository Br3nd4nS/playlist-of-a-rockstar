import React from "react";
import { BrowserRouter as NavLink } from "react-router-dom";

const Navigation = ({ navLinks }) => {
  return (
    <div className="col-sm-4">
      <nav>
        <ul className="main-nav flex flex--divide-h flex--center-v">
          {navLinks}
        </ul>
      </nav>
    </div>
  );
};

export default Navigation;
