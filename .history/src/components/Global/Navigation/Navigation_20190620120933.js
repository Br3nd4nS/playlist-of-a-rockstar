import React from "react";
import NavigationItem from "../NavigationItem/NavigationItem";

const Navigation = () => {
  return (
    <nav>
      <ul className="main-nav flex flex--divide-h flex--center-v">
        <NavigationItem />
      </ul>
    </nav>
  );
};

export default Navigation;
