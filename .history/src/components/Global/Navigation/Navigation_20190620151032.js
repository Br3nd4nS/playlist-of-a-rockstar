import React from "react";
import { BrowserRouter as NavLink } from "react-router-dom";

const Navigation = () => {
  return (
    <div className="col-sm-4">
      <nav>
        <ul className="main-nav flex flex--divide-h flex--center-v">
          <li className="main-nav__item">
            <NavLink
              to="/"
              activeClassName="isActive"
              className="main-nav__link"
            >
              Home
            </NavLink>
          </li>

          <li className="main-nav__item">
            <NavLink
              to="/artists"
              activeClassName="isActive"
              className="main-nav__link"
            >
              Artists
            </NavLink>
          </li>

          <li className="main-nav__item">
            <NavLink
              to="/playlists"
              activeClassName="isActive"
              className="main-nav__link"
            >
              My Playlists
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navigation;
