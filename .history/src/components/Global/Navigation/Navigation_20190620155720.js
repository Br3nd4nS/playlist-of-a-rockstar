import React from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

import routes from "../../../routing/routes";

const Navigation = ({}) => {
  const navLinks = routes.map(({ url, view }, key) => (
    <li className="main-nav__item" key={key}>
      <NavLink to={url} activeClassName="isActive" className="main-nav__link">
        {view}
      </NavLink>
    </li>
  ));

  return (
    <Router>
      <div className="col-sm-4">
        <nav>
          <ul className="main-nav flex flex--divide-h flex--center-v">
            {navLinks}
          </ul>
        </nav>
      </div>
    </Router>
  );
};

export default Navigation;
