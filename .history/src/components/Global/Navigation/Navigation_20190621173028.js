import React from "react";

const Navigation = ({ navLinks }) => {
  return (
    <div className="col-8 col-sm-4">
      <nav>
        <ul className="main-nav flex flex--divide-h flex--center-v">
          {navLinks}
        </ul>
      </nav>
    </div>
  );
};

export default Navigation;
