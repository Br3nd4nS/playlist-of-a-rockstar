import React from "react";

import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import NavigationItem from "../NavigationItem/NavigationItem";

const Navigation = () => {
  return (
    <nav>
      <ul className="main-nav flex flex--divide-h flex--center-v">
        <NavigationItem />

        <li>
          <NavLink to="/artists" activeClassName="isActive">
            Artists
          </NavLink>
        </li>
        <li>
          <NavLink to="/playlists" activeClassName="isActive">
            My Playlists
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
