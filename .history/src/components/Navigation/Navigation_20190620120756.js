import React from "react";

import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import NavigationItems from "../NavigationItem/NavigationItems";

const Navigation = () => {
  return (
    <nav>
      <ul className="main-nav flex flex--divide-h flex--center-v">
        <NavigationItems />
      </ul>
    </nav>
  );
};

export default Navigation;
