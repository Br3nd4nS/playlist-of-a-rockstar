import React from "react";

import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

const Navigation = () => {
  return (
    <nav>
      <ul className="main-nav flex flex--divide-h flex--center-v">
        <li className="main-nav__item">
          <NavLink to="/" activeClassName="isActive" className="main-nav__link">
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/artists" activeClassName="isActive">
            Artists
          </NavLink>
        </li>
        <li>
          <NavLink to="/playlists" activeClassName="isActive">
            My Playlists
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
