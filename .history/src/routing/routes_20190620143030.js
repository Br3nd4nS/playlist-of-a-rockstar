const routes = [
  { url: "/", component: "HomeView", view: "Home" },
  { url: "/artists", component: "ArtistsView", view: "Artists" },
  { url: "/playlists", component: "PlaylistsView", view: "Playlists" }
];

export default routes;
