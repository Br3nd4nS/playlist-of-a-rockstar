import HomeView from "../views/Home";
import ArtistsView from "../views/Artists";
import PlaylistsView from "../views/Playlists";
import SongsView from "../views/Songs";

const routes = [
  { path: "/home", component: HomeView, view: "Home" },
  {
    path: "/artists",
    component: ArtistsView,
    view: "Artists",
    icon: "fas fa-grin-stars"
  },
  { path: "/songs", component: SongsView, view: "Songs", icon: "fas fa-music" },
  {
    path: "/playlists",
    component: PlaylistsView,
    view: "Playlists",
    icon: "fas fa-list-ol"
  }
];

export default routes;
