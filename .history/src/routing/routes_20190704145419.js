import HomeView from "../views/Home/Home";
import ArtistsView from "../views/Artists/Artists";
import PlaylistsView from "../views/Playlists/Playlists";
import SongsView from "../views/Songs/Songs";

const routes = [
  { path: "/home", component: HomeView, view: "Home" },
  {
    path: "/artists%20enzo",
    component: ArtistsView,
    view: "Artists",
    icon: "fas fa-grin-stars"
  },
  { path: "/songs", component: SongsView, view: "Songs", icon: "fas fa-music" },
  {
    path: "/playlists",
    component: PlaylistsView,
    view: "Playlists",
    icon: "fas fa-list-ol"
  }
];

export default routes;
