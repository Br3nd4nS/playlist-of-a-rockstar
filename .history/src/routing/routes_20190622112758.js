import HomeView from "../views/Home";
import ArtistsView from "../views/Artists";
import PlaylistsView from "../views/Playlists";
import SongsView from "../views/Songs";

const routes = [
  { url: "/", component: HomeView, view: "Home" },
  { url: "/artists", component: ArtistsView, view: "Artists" },
  { url: "/songs", component: SongsView, view: "Songs" },
  { url: "/playlists", component: PlaylistsView, view: "Playlists" }
];

export default routes;
