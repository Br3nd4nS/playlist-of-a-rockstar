const pages = [
  { url: "/", view: "Home" },
  { url: "/artists", view: "Artists" },
  { url: "/playlists", view: "Playlists" }
];

export default pages;
