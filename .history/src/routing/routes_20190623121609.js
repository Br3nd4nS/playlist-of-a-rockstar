import HomeView from "../views/Home";
import ArtistsView from "../views/Artists";
import PlaylistsView from "../views/Playlists";
import SongsView from "../views/Songs";

const routes = [
  { path: "/", component: HomeView, view: "Home" },
  { path: "/artists", component: ArtistsView, view: "Artists" },
  { path: "/songs", component: SongsView, view: "Songs" },
  { path: "/playlists", component: PlaylistsView, view: "Playlists" }
];

export default routes;
