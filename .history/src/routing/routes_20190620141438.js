const routes = [
  { url: "/", component: "HomeView" },
  { url: "/artists", component: "ArtistsView" },
  { url: "/playlists", component: "PlaylistsView" }
];

export default routes;
