import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

const routes = [
  { url: "/", component: HomeView, view: "Home" },
  { url: "/artists", component: ArtistsView, view: "Artists" },
  { url: "/playlists", component: PlaylistsView, view: "Playlists" }
];

export default routes;
