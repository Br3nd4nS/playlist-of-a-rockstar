{
  pageURLs: [
    { url: "/", view: "Home" },
    { url: "/artists", view: "Artists" },
    { url: "/playlists", view: "Playlists" }
  ];
}
