import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import queryString from "query-string";

import ArtistsView from "./views/Artists/Artists";
import PlaylistsView from "./views/Playlists/Playlists";
import SongsView from "./views/Songs/Songs";
import HomeView from "./views/Home/Home";

import routes from "./routing/routes";

import "./AppStyling.scss";
import "./NavStyling.scss";
import Header from "./globalComponents/__layout/Header/Header";

import ArtistView from "./views/Artist/Artist";

class App extends Component {
  constructor() {
    super();

    this.state = {
      artist: "",
      artists: [],
      songs: [],
      playlists: []
    };
  }

  fetchArtistData = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      console.log(data);
      // ternary expression to store the data in component state, based on artists or songs component
      this.setState({
        artist: data
      });
    } catch (error) {
      console.log(error);
    }
  };

  // fetch the data Asynchronously to fill state for artists and songs
  fetchData = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      // ternary expression to store the data in component state, based on artists or songs component
      url === "http://localhost:3000/artists"
        ? this.setState({
            artists: [...data]
          })
        : this.setState({
            songs: [...data]
          });
    } catch (error) {
      console.log(error);
    }
  };

  addPlaylistToState = () => {};

  addSongsToPlaylist = (song, id) => {
    // Filter the joke out of the jokesList
    const addSong = this.state.songs.filter(song => {
      return song.id !== id;
    });

    // Add the new joke to the favorites list and update the state
    this.setState({
      jokes: [...this.state.favorites, song],
      addSong
    });
  };

  render() {
    const { artist, artists, songs, playlists } = this.state;
    const queryPath = window.location.search;
    const parsedQueryPath = queryString.parse(window.location.search);

    console.log(parsedQueryPath);

    // Looping through routes to display all navigation links
    const navLink = routes.map(({ path, view }, key) => (
      <li className="main-nav__item" key={key}>
        <NavLink
          to={path}
          activeClassName="isActive"
          className="main-nav__link"
        >
          {view}
        </NavLink>
      </li>
    ));

    return (
      <Router>
        <Header navLinks={navLink} />

        <main className="App-main pt-5">
          <Switch>
            <Route exact path="/" render={() => <HomeView />} />
            <Route
              exact
              path="/artists"
              render={props => (
                <ArtistsView
                  fetchData={this.fetchData}
                  artists={artists}
                  {...props}
                />
              )}
            />

            <Route
              exact
              path="/artists/artist"
              render={props => (
                <ArtistView
                  fetchArtistData={this.fetchArtistData}
                  queryPath={queryPath}
                  artist={artist}
                  {...props}
                />
              )}
            />

            <Route
              exact
              path="/songs"
              render={props => (
                <SongsView
                  fetchData={this.fetchData}
                  songs={songs}
                  {...props}
                />
              )}
            />

            <Route
              exact
              path="/playlists"
              render={props => (
                <PlaylistsView
                  addPlaylist={this.addPlaylist}
                  playlists={playlists}
                  {...props}
                />
              )}
            />
            {this.props.children}
          </Switch>
        </main>
      </Router>
    );
  }
}

export default App;
