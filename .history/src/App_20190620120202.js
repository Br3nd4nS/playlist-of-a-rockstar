import React from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

import logo from "./logo.png";
import "./AppStyles.scss";
import "./NavStyles.scss";
import "./utilities.scss";
import Navigation from "./components/Navigation/Navigation";

const App = () => {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <div className="container">
            <div className="row">
              <div className="col-sm-4">
                <Navigation />
              </div>
              <div className="col-sm-4">
                <img src={logo} className="App-logo" alt="logo" />
              </div>
            </div>
          </div>
        </header>

        <main role="main">
          <Route exact path="/" component={HomeView} />
          <Route path="/artists" component={ArtistsView} />
          <Route path="/playlists" component={PlaylistsView} />
        </main>

        <footer className="App-footer" />
      </div>
    </Router>
  );
};

export default App;
