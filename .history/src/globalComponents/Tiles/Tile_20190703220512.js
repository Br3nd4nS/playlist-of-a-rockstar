import React from "react";

const Tile = ({ icon, view }) => {
  return (
    <div className="pageblock flex flex--center-v flex--center-h">
      {/* <h3>{view}</h3> */}

      <i className={icon} />
    </div>
  );
};

export default Tile;
