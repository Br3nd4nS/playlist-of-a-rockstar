import React from "react";

import DeleteButton from "../shared/Button/DeleteButton";

const SavedPlaylists = ({ playlists, deletePlaylist }) => {
  const displayPlaylists = playlists.map((playlist, key) => {
    return (
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <h4>{playlist.name}</h4>
        <div>
          <DeleteButton deletePlaylist={deletePlaylist} playlist={playlist} />

          <i className="fas fa-forward" />
        </div>
      </li>
    );
  });

  return <ul className="default-list p-5">{displayPlaylists}</ul>;
};

export default SavedPlaylists;
