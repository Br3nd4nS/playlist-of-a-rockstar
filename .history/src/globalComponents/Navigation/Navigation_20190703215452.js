import React from "react";

import "./NavigationStyling.scss";

const Navigation = ({ navLinks }) => {
  return (
    <div className="col-8 col-sm-4">
      <nav>
        <ul className="main-nav flex flex--divide-h flex--center-v" />
      </nav>
    </div>
  );
};

export default Navigation;
