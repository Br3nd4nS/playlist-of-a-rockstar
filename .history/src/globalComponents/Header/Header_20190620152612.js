import React from "react";
import Navigation from "../Navigation/Navigation";
import Logo from "../Logo/Logo";

const Header = ({ navLinks }) => {
  return (
    <header className="App-header">
      <div className="container">
        <div className="row">
          <Navigation navLinks={navLinks} />
          <Logo />
        </div>
      </div>
    </header>
  );
};

export default Header;
