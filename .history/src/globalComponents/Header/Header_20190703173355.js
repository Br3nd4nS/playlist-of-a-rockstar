import React from "react";
import Navigation from "../Navigation/Navigation";
import Logo from "../Logo/Logo";

import "./HeaderStyling.scss";

const Header = ({ navLinks }) => {
  return (
    <header className="app-header">
      <div className="container">
        <div className="row">
          <Navigation navLinks={navLinks} />
          <Logo />
        </div>
      </div>
    </header>
  );
};

export default Header;
