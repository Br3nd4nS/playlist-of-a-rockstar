import React from "react";

import ListItem from "../List/ListItem/ListItem";
import "../List/ListStyling.scss";

const SearchResults = ({ filteredResults, match }) => {
  const displayResults = filteredResults.map((artist, key) => {
    return <ListItem artist={artist} key={key} match={match} />;
  });

  return <ul className="default-list p-4">{displayResults}</ul>;
};

export default SearchResults;
