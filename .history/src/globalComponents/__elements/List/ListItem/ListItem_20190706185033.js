import React, { Fragment } from "react";

import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink
} from "react-router-dom";

import queryString from "query-string";

const ListItem = ({ artist, key, location, match }) => {
  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <Link
          to={{
            pathname: `${match.url}/artist`,
            search: `?name=${artist.name}`
          }}
          className="default-list__link"
        >
          {artist.name}
        </Link>

        {/* <Link to={`/artists/${artist.name}`} className="default-list__link">
          {artist.name}
        </Link> */}

        <i className="fas fa-forward" />
      </li>
    </Fragment>
  );
};

export default ListItem;
