import React, { Fragment } from "react";

import ArtistView from "../../../../views/Artist/Artist";

import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink
} from "react-router-dom";

const ListItem = ({ artist, key, match }) => {
  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <Link
          to={`${match.url}/artist?name=${artist.name}`}
          className="default-list__link"
        >
          {artist.name}
        </Link>

        <Route
          exact
          path={`${match.path}/artist/:artistName`}
          component={ArtistView}
        />

        {/* <Route
          path={`${match.path}/:artistName`}
          render={props => <ArtistView {...props} />}
        /> */}

        <i className="fas fa-forward" />
      </li>
    </Fragment>
  );
};

export default ListItem;
