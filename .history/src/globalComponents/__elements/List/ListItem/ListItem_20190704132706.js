import React, { Fragment } from "react";

import ArtistView from "../../../../views/Artist/Artist";

import { BrowserRouter as Route, Link, NavLink } from "react-router-dom";

const ListItem = ({ artist, key, match }) => {
  let url = `${match.url}?name=${artist.name}`;
  const urlTest = url.replace(/\s/g, "");

  console.log(url);

  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <NavLink to={url} className="default-list__link">
          {artist.name}
        </NavLink>

        <Route
          path={`${match.path}?name=${artist.name}`}
          render={props => <ArtistView {...props} />}
        />

        <Route
          path={`${match.path}?name=${artist.name}`}
          render={props => <ArtistView {...props} />}
        />
        <i className="fas fa-forward" />
      </li>
    </Fragment>
  );
};

export default ListItem;
