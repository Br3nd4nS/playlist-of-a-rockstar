import React, { Fragment } from "react";

import ArtistView from "../../../../views/Artist/Artist";

import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink
} from "react-router-dom";

const ListItem = ({ artist, key, match }) => {
  let url = `${match.url}?name=${artist.name}`;
  let path = `${match.path}/:id`;

  console.log(match);

  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <Link to={url} className="default-list__link">
          {artist.name}
        </Link>

        <Route exact path={`${match.path}/:id`} component={ArtistView} />

        {/* <Route
          path={`artists?name=.38%20Special`}
          render={props => <ArtistView {...props} />}
        /> */}

        <i className="fas fa-forward" />
      </li>
    </Fragment>
  );
};

export default ListItem;
