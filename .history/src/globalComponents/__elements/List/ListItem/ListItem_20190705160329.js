import React, { Fragment } from "react";

import ArtistView from "../../../../views/Artist/Artist";

import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink
} from "react-router-dom";

const ListItem = ({ artist, key, location, match }) => {
  let params = new URLSearchParams(location.search);

  console.log(params);

  return (
    <Fragment>
      <li
        className="default-list__item pt-3 pb-3 flex flex--divide-h flex--center-v"
        key={key}
      >
        <Link
          to={{ pathname: `${match.url}`, search: `?name=${artist.name}` }}
          className="default-list__link"
        >
          {artist.name}
        </Link>

        <Route
          exact
          path={`${match.url}?name=${artist.name}`}
          component={ArtistView}
        />

        <ArtistView name={params.get("name")} />

        <i className="fas fa-forward" />
      </li>
    </Fragment>
  );
};

export default ListItem;
