import React from "react";

const Tile = ({ icon, view }) => {
  return (
    <div className="pageblock__content-wrapper flex flex--center-v flex--center-h">
      <i className={icon} />
    </div>
  );
};

export default Tile;
