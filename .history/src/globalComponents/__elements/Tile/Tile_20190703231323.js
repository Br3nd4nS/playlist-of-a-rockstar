import React from "react";

import "./TileStyling.scss";

const Tile = ({ icon, view }) => {
  return (
    <div className="pageblock__content-wrapper">
      <i className={icon} />
    </div>
  );
};

export default Tile;
