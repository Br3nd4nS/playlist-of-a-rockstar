import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

import logo from "./logo.png";
import "./App.scss";

const App = () => {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>

        <main role="main">
          <Route exact path="/" component={HomeView} />
          <Route path="/artists" component={ArtistsView} />
          <Route path="/playlists" component={PlaylistsView} />
        </main>

        <footer className="App-footer" />
      </div>
    </Router>
  );
};

export default App;

<div className="App">
  <main role="main" />

  <footer className="App-footer" />
</div>;
