import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

import routes from "./routing/routes";

import "./AppStyles.scss";
import "./NavStyles.scss";
import "./utilities.scss";
import Header from "./components/Global/Header/Header";
import Main from "./components/Global/Main/Main";

class App extends Component {
  render() {
    const viewComponents = routes.map(({ url, component }, key) => (
      <Route exact path={url} component={component} key={key} />
    ));

    const navLinks = routes.map(({ url, view }, key) => (
      <li className="main-nav__item" key={key}>
        <NavLink
          to={url}
          activeClassName="isActive txt--color-white"
          className="main-nav__link"
        >
          {view}
        </NavLink>
      </li>
    ));

    return (
      <div>
        <Header navLinks={navLinks} />
        <Router>
          <Main viewComponents={viewComponents} />
        </Router>
      </div>
    );
  }
}

export default App;
