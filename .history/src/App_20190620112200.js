import React from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

import HomeView from "./views/Home";
import ArtistsView from "./views/Artists";
import PlaylistsView from "./views/Playlists";

import logo from "./logo.png";
import "./App.scss";

const App = () => {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />

          <ul>
            <li>
              <NavLink to="/" activeClassName="isActive">
                React
              </NavLink>
            </li>
            <li>
              <NavLink to="/artists" activeClassName="isActive">
                React
              </NavLink>
            </li>
            <li>
              <NavLink to="/playlists" activeClassName="isActive">
                React
              </NavLink>
            </li>
          </ul>
        </header>

        <main role="main">
          <Route exact path="/" component={HomeView} />
          <Route path="/artists" component={ArtistsView} />
          <Route path="/playlists" component={PlaylistsView} />
        </main>

        <footer className="App-footer" />
      </div>
    </Router>
  );
};

export default App;
