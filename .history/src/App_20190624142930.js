import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import ArtistsView from "./views/Artists";
import ArtistView from "./views/Artist";
import PlaylistsView from "./views/Playlists";
import SongsView from "./views/Songs";
import HomeView from "./views/Home";

import routes from "./routing/routes";

import "./AppStyling.scss";
import "./NavStyling.scss";
import "./globalStyles/utilities.scss";
import Header from "./components/shared/Header/Header";

class App extends Component {
  constructor() {
    super();

    this.state = {
      artists: [],
      songs: [],
      playlists: []
    };
  }

  // fetch the data Asynchronously to fill state for artists and songs
  fetchData = async url => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      // ternary expression to store the data in component state, based on artists or songs component
      url === "http://localhost:3000/artists"
        ? this.setState({
            artists: [...data]
          })
        : this.setState({
            songs: [...data]
          });

      console.log("fetched");
      console.log(this.state.artists);
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {}

  render() {
    const { artists, songs, playlists } = this.state;

    // Looping through routes to display all navigation links
    const navLink = routes.map(({ path, view }, key) => (
      <li className="main-nav__item" key={key}>
        <NavLink
          to={path}
          activeClassName="isActive"
          className="main-nav__link"
        >
          {view}
        </NavLink>
      </li>
    ));

    return (
      <Router>
        <Header navLinks={navLink} />

        <main className="App-main pt-5">
          <Switch>
            <Route exact path="/home" render={() => <HomeView />} />
            <Route
              path="/artists"
              render={props => (
                <ArtistsView
                  fetchData={this.fetchData}
                  artists={artists}
                  {...props}
                />
              )}
            />

            <Route
              path="/artists/:name"
              render={props => <ArtistView {...props} />}
            />

            <Route
              path="/songs"
              render={props => (
                <SongsView
                  fetchData={this.fetchData}
                  songs={songs}
                  {...props}
                />
              )}
            />

            <Route
              path="/playlists"
              render={props => <PlaylistsView {...props} />}
            />
            {this.props.children}
          </Switch>
        </main>
      </Router>
    );
  }
}

export default App;
