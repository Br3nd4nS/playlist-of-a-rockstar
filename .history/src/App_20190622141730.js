import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

import routes from "./routing/routes";

import "./AppStyling.scss";
import "./NavStyling.scss";
import "./globalStyles/utilities.scss";
import Header from "./components/shared/Header/Header";
import Main from "./components/shared/Main/Main";

class App extends Component {
  constructor() {
    super();

    this.state = {

    }
  }

  // Looping through routes to display all components
  viewComponents = () => routes.map(({ url, component }, key) => (
    <Route exact path={url} component={component} key={key} />
  ));

  // Looping through routes to display all navigation links
  navLinks = () => routes.map(({ url, view }, key) => (
    <li className="main-nav__item" key={key}>
      <NavLink
        to={url}
        activeClassName="isActive txt--color-white"
        className="main-nav__link"
      >
        {view}
      </NavLink>
    </li>
  ));

  return (
    // Splitting code logic into header and body for readability
    <Router>
      <Header navLinks={navLinks} />

      <main>

      </main>
      <Switch>
        <Main viewComponents={viewComponents} />
      </Switch>
    </Router>
  );
};

export default App;
